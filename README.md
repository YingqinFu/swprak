# Schach

Ein spaceiges digitales Schachspiel geschrieben in Java 11 unter anderem mit
folgenden Features:

 - Konsolenbasierte Benutzerschnittstelle
 - Grafische Benutzeroberfläche (noch in Arbeit)
 - "Mensch gegen Mensch"-Spiele
 - "Mensch gegen Computer"-Spiele (noch in Arbeit)
 - Netzwerkspiel (noch in Arbeit)

## Für Benutzer...

...gibt es eine Bedienungsanleitung in der Datei "Bedienungsanleitung.pdf".

## Für Entwickler...

...gibt es in der Bedienungsanleitung eine Buildanleitung ebenfalls in der
Bedienungsanleitung.


# Maven

Kurzübersicht nützlicher Maven-Befehle. Weitere Informationen finden sich im Tutorial:

* `mvn clean` löscht alle generierten Dateien
* `mvn compile` übersetzt den Code
* `mvn javafx:jlink` packt den gebauten Code als modulare Laufzeit-Image. Das Projekt kann danach gestartet werden mit `target/chess/bin/chess`
* `mvn test` führt die Tests aus
* `mvn compile site` baut den Code, die Dokumentation und die Tests und führt alle Tests, sowie JaCoCo und PMD inklusive CPD aus. Die Datei `target/site/index.html` bietet eine Übersicht über alle Reports.
* `mvn javafx:run` führt das Projekt aus
* `mvn javafx:run -Dargs="--no-gui"` führt das Projekt mit Command-Line-Parameter `--no-gui` aus.
