package chess.controller;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import chess.model.ChessBoard;
import chess.model.Color;
import chess.model.Pawn;
import chess.model.Rook;
/**
 * Test class for Input class
 * @author Yingqin Fu, Steffen Häusler, Christoph Cohrs-Thiede from Gruppe 20 
 *
 */
public class InputTest {
	/**
	 * test for invalid input
	 */
	@Test
	public void testinvalidInput() {
		Input input =new Input();
		
		
		input.setInput("11-11");
		assertTrue(Input.invalidInput(input), "invalid input1");
		
		input.setInput("x");
		assertTrue(Input.invalidInput(input), "invalid input2");
		
		input.setInput("e2-e9");
		assertTrue(Input.invalidInput(input), "invalid input3");
		
		input.setInput("e2 e4");
		assertTrue(Input.invalidInput(input), "invalid input4");
		
		input.setInput("e2-e4");
		assertFalse(Input.invalidInput(input), "valid input");
		
		
	}
	
	
	
	/**
	 * test for invalid input 
	 */
	@Test
	public void testinvalidInputWithPromotion() {
		Input input =new Input();
		
		
		input.setInput("11-11");
		assertTrue(Input.invalidInputWithPromotion(input), "invalid input promotion 1");
		
		input.setInput("x");
		assertTrue(Input.invalidInputWithPromotion(input), "invalid input promotion 2");
		
		
		input.setInput("e2-e4");
		assertFalse(Input.invalidInputWithPromotion(input), "valid input 1");
		
		input.setInput("e2-e4R");
		assertFalse(Input.invalidInputWithPromotion(input), "valid input 2");
		
		input.setInput("e2-e4B");
		assertFalse(Input.invalidInputWithPromotion(input), "valid input 3");
		
		
	}
	
	/**
	 * test for beaten input
	 */
	@Test
	public void testBeaten() {
		ChessBoard board= new ChessBoard();
		
		boolean[][] potentialMove = new boolean[8][8];
		for (int j = 0; j < 8; j++){
			for (int k = 0; k < 8; k++){
				potentialMove[j][k] = false;
			}
		}
		Pawn PB = new Pawn(4,4,Color.black, potentialMove);
		Rook RW = new Rook(5,4,Color.white,potentialMove);
		board.setFigure(4, 4, PB);
		board.setFigure(5, 4, RW);
		
		Input input =new Input();
		input.setInput("f4-e4");
		RunGame.makeChessMove(board, input);
		 for (String i : board.getBeatenList()) {
			 assertEquals("p", i, "false color");
	        }
		
		
		
		
	}
	
	

}
