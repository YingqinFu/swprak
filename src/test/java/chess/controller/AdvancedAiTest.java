package chess.controller;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import chess.model.ChessBoard;
import chess.model.Color;
import chess.model.Knight;
/**
 * test for advanced AI
 * @author Yingqin Fu
 *
 */
public class AdvancedAiTest {
	/**
	 * test 1 for MinMax
	 */
	@Test
	public void testMinMax1() {
		ChessBoard board =new ChessBoard();
		AdvancedAI ai=new AdvancedAI(Color.white,1);
		
		boolean[][] potentialMove = new boolean[8][8];
		for (int j = 0; j < 8; j++){
			for (int k = 0; k < 8; k++){
				potentialMove[j][k] = false;
			}
		}
		Knight NW1 = new Knight(4,4,Color.white, potentialMove);
		board.setFigure(4, 4, NW1);
		
		
		assertEquals(ai.getBestMove(board).charAt(0), ai.minimaxChess(board,1,0,"",Integer.MIN_VALUE,Integer.MAX_VALUE)[0].charAt(0));
	}
	
	/**
	 * test 2 for MinMax
	 */
	@Test
	public void testMinMax2() {
		ChessBoard board =new ChessBoard();
		AdvancedAI ai=new AdvancedAI(Color.black,2);
		
		boolean[][] potentialMove = new boolean[8][8];
		for (int j = 0; j < 8; j++){
			for (int k = 0; k < 8; k++){
				potentialMove[j][k] = false;
			}
		}
		Knight NW1 = new Knight(4,4,Color.white, potentialMove);
		board.setFigure(4, 4, NW1);
		
		assertEquals(ai.getBestMove(board),"a1-a1");
	}
	
	/**
	 * test 3 for MinMax
	 */
	@Test
	public void testMinMax3() {
		
		ChessBoard board =new ChessBoard();
		AdvancedAI ai=new AdvancedAI(Color.white,5);
		
		boolean[][] potentialMove = new boolean[8][8];
		for (int j = 0; j < 8; j++){
			for (int k = 0; k < 8; k++){
				potentialMove[j][k] = false;
			}
		}
		Knight NW1 = new Knight(4,4,Color.white, potentialMove);
		
		board.setFigure(4, 4, NW1);
		
		assertEquals(ai.minimaxChess(board, 2, 0, "",Integer.MIN_VALUE,Integer.MAX_VALUE)[0].charAt(0),'e');
		assertEquals(ai.minimaxChess(board, 2, 0, "",Integer.MIN_VALUE,Integer.MAX_VALUE)[0].charAt(1),'4');
		
	}
	
	/**
	 * test 3 for MinMax
	 */
	@Test
	public void testMinMax4() {
		
		ChessBoard board =new ChessBoard();
		AdvancedAI ai=new AdvancedAI(Color.black,3);
		
		boolean[][] potentialMove = new boolean[8][8];
		for (int j = 0; j < 8; j++){
			for (int k = 0; k < 8; k++){
				potentialMove[j][k] = false;
			}
		}
		Knight NB1 = new Knight(4,4,Color.black, potentialMove);
		board.setFigure(4, 4, NB1);
		Knight NW1 = new Knight(5,5,Color.white, potentialMove);
		board.setFigure(5,5, NW1);
	
		assertEquals(ai.getBestMove(board).charAt(0),'e');
		assertEquals(ai.getBestMove(board).charAt(1),'4');
		
	}
	/**
	 * test for color
	 */
	@Test
	public void testColor() {
		AdvancedAI ai=new AdvancedAI(Color.black,3);
		ai.setColor(Color.white);
		assertEquals(Color.white,ai.getColor());
	}
	/**
	 * test for level
	 */
	@Test
	public void testLevel() {
		AdvancedAI ai=new AdvancedAI(Color.black,3);
		ai.setLevel(2);
		assertEquals(2,ai.getLevel());
	}
	
	

}
