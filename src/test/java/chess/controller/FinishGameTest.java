package chess.controller;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import chess.model.ChessBoard;
/**
 * the test class for FinishGame
 * @author Yingqin Fu, Steffen Häusler, Christoph Cohrs-Thiede from Gruppe 20
 *
 */
public class FinishGameTest {
/**
 * test for finish 
 */
	@Test
	public void testFinish() {
		ChessBoard board =new ChessBoard();
		StartGame.initLineup(board);
		
		assertTrue(FinishGame.whiteKingAlive(board), "white king alive");
		
		assertTrue(FinishGame.blackKingAlive(board), "black king alive");
	}

}
