package chess.controller;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import chess.model.ChessBoard;
import chess.model.Color;
import chess.model.King;
import chess.model.Pawn;
import chess.model.Rook;
/**
 * Test class for Rules class
 * @author Yingqin Fu, Steffen Häusler, Christoph Cohrs-Thiede from Gruppe 20 
 *
 */
public class RulesTest {
	/**
	 * test for noRuleBroken
	 */
	@Test
	public void testnoRuleBroken() {
		ChessBoard board =new ChessBoard();
		
		
		boolean[][] potentialMove = new boolean[8][8];
		for (int j = 0; j < 8; j++){
			for (int k = 0; k < 8; k++){
				potentialMove[j][k] = false;
			}
		}
		Pawn PW = new Pawn(4,6,Color.white, potentialMove);
		board.setFigure(4, 6, PW);
		
		Input input = new Input();
		
		
		input.setInput("e2-e4");
		assertTrue(Rules.noRuleBroken(board, input), "correct move");
		
		input.setInput("e2-e5");
		assertFalse(Rules.noRuleBroken(board, input), "incorrect move");
	}
	
	
	/**
	 * test for KingInCheck
	 */
	@Test
	public void testWhiteKingInCheck() {
		ChessBoard board =new ChessBoard();
		
		
		boolean[][] potentialMove = new boolean[8][8];
		for (int j = 0; j < 8; j++){
			for (int k = 0; k < 8; k++){
				potentialMove[j][k] = false;
			}
		}
		King KW = new King(4,6,Color.white, potentialMove);
		Rook RW =new Rook(4,5,Color.white, potentialMove);
		Rook RB =new Rook(4,4,Color.black, potentialMove);
		board.setFigure(4, 6, KW);
		board.setFigure(4, 5, RW);
		board.setFigure(4, 4, RB);
		
		Input input = new Input();
		
		input.setInput("e3-d3");
		assertTrue(Rules.checkCheck(board, input)[0], "white King in check");
		
		input.setInput("e2-d2");
		assertFalse(Rules.checkCheck(board, input)[0], "white King not in check");
	}
	

	/**
	 * test for KingInCheck
	 */
	@Test
	public void testFinishGame() {
		boolean[][] potentialMove = new boolean[8][8];
		for (int j = 0; j < 8; j++){
			for (int k = 0; k < 8; k++){
				potentialMove[j][k] = false;
			}
		}
		ChessBoard board =new ChessBoard();
		StartGame.initFigures(board, potentialMove);
		assertFalse(Rules.gameFinished(board, Color.white));
		
	}
	/**
	 * test for KingInCheck
	 */
	@Test
	public void testBlackKingInCheck() {
		ChessBoard board =new ChessBoard();
		
		
		boolean[][] potentialMove = new boolean[8][8];
		for (int j = 0; j < 8; j++){
			for (int k = 0; k < 8; k++){
				potentialMove[j][k] = false;
			}
		}
		King KB = new King(4,6,Color.black, potentialMove);
		Rook RB =new Rook(4,5,Color.black, potentialMove);
		Rook RW =new Rook(4,4,Color.white, potentialMove);
		board.setFigure(4, 6, KB);
		board.setFigure(4, 5, RB);
		board.setFigure(4, 4, RW);
		
		Input input = new Input();
		
		input.setInput("e3-d3");
		assertTrue(Rules.checkCheck(board, input)[0], "black King in check");
		
		input.setInput("e2-d2");
		assertTrue(Rules.checkCheck(board, input)[0], "black King not in check");
	}

	/**
	 * test for remi
	 */
	@Test
	public void testRemi() {
		ChessBoard board =new ChessBoard();
		board.setMovedList("e2-e3");
		board.setMovedList("e3-e2");
		board.setMovedList("e2-e3");
		board.setMovedList("e3-e2");
		board.setMovedList("e2-e4");
		board.setMovedList("e3-e4");
		board.setMovedList("e2-e4");
		board.setMovedList("e3-e4");
		board.setMovedList("e2-e5");
		board.setMovedList("e3-e5");
		board.setMovedList("e2-e5");
		board.setMovedList("e3-e5");
		
		assertFalse(Rules.remi(board));
		
	}



}

