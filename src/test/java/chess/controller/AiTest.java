package chess.controller;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import chess.model.ChessBoard;
import chess.model.Color;
import chess.model.Figure;
import chess.model.Knight;
import chess.model.Rook;

/**
 * the test for Simple Ai
 * @author Yingqin Fu
 *
 */
public class AiTest {
	/**
	 * test for movable
	 */
	@Test
	 public void testMoveable() {
		boolean[][] potentialMove = new boolean[8][8];
		for (int j = 0; j < 8; j++){
			for (int k = 0; k < 8; k++){
				potentialMove[j][k] = false;
			}
		}
		SimpleAI ai =new SimpleAI();
		ChessBoard board =new ChessBoard();
		StartGame.initFigures(board, potentialMove);
		
		 assertTrue(ai.moveable(board, 4, 6));
		
	}
	/**
	 * test for movable
	 */
	@Test
	public void testMovable() {
		boolean[][] potentialMove = new boolean[8][8];
		for (int j = 0; j < 8; j++){
			for (int k = 0; k < 8; k++){
				potentialMove[j][k] = false;
			}
		}
		SimpleAI ai =new SimpleAI();
		ChessBoard board =new ChessBoard();
		Knight NW1 = new Knight(4,4,Color.white, potentialMove);
		board.setFigure(4, 4, NW1);
		ai.setColor(Color.white);
		//System.out.println(ai.getRandomMove(board));
		//System.out.println(ai.randomMove(board));
		assertEquals(ai.randomMove(board).charAt(0),ai.getRandomMove(board).charAt(0));
	}
	
	/**
	 * test for a random move
	 */
	@Test
	 public void testBestMove() {
		boolean[][] potentialMove = new boolean[8][8];
		for (int j = 0; j < 8; j++){
			for (int k = 0; k < 8; k++){
				potentialMove[j][k] = false;
			}
		}
		SimpleAI ai =new SimpleAI();
		ChessBoard board =new ChessBoard();
		StartGame.initFigures(board, potentialMove);
		
		
		assertEquals("",ai.bestFigureMove(board, 3, 1));
	}
	/**
	 * test for color
	 */
	@Test
	 public void testColor() {
		Color color=Color.white;
		SimpleAI ai =new SimpleAI();
		ai.setColor(color);
		assertEquals(ai.getColor().toString(),"white");
		
		
	}
	/**
	 * test for the random move
	 */
	@Test
	 public void testRandomMove() {
		
		boolean[][] potentialMove = new boolean[8][8];
		for (int j = 0; j < 8; j++){
			for (int k = 0; k < 8; k++){
				potentialMove[j][k] = false;
			}
		}
		SimpleAI ai =new SimpleAI();
		ChessBoard board =new ChessBoard();
		StartGame.initFigures(board, potentialMove);
		board.setFigureNull(4, 6);
		
		assertEquals("e1-e2",ai.randomMove(board, 4, 7));
		
		
	}
	
	
	/**
	 * test for getBestMove
	 */
	@Test
	 public void testBestMove2() {
		boolean[][] potentialMove = new boolean[8][8];
		for (int j = 0; j < 8; j++){
			for (int k = 0; k < 8; k++){
				potentialMove[j][k] = false;
			}
		}
		SimpleAI ai =new SimpleAI();
		ChessBoard board =new ChessBoard();
		
		for (int j = 0; j < 8; j++){
			for (int k = 0; k < 8; k++){
				Rook rook=new Rook(j, k, Color.white, potentialMove);
				board.setFigure(j, k, rook);
			}
		}
		
		
		ai.getBestMove(board);
		
		
		assertEquals("a1-a1",ai.getBestMove(board));
		
	}
	
	/**
	 * test for getBestMove
	 */
	@Test
	 public void testCopy() {
		boolean[][] potentialMove = new boolean[8][8];
		for (int j = 0; j < 8; j++){
			for (int k = 0; k < 8; k++){
				potentialMove[j][k] = false;
			}
		}
		ChessBoard board =new ChessBoard();
		StartGame.initBlackFigures(board, potentialMove);
		Figure[][] copyFigures=new Figure[8][8];
		SimpleAI ai=new SimpleAI();
		ai.copyFigures(board, copyFigures);
		assertEquals(board.getFigure(0, 0).getName(),"r");
	}
	/**
	 * test for best move
	 */
	@Test
	 public void testBestMove3() {
		boolean[][] potentialMove = new boolean[8][8];
		for (int j = 0; j < 8; j++){
			for (int k = 0; k < 8; k++){
				potentialMove[j][k] = false;
			}
		}
		SimpleAI ai =new SimpleAI();
		ChessBoard board =new ChessBoard();
		
		
		Rook rook=new Rook(4, 4, Color.white, potentialMove);
		board.setFigure(4, 4, rook);
		
		ai.setColor(Color.white);
		assertEquals(ai.getBestMove(board).charAt(0),ai.randomMove(board).charAt(0));
		
		
	}
}
