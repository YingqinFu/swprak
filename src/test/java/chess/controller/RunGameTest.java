package chess.controller;

import static org.junit.jupiter.api.Assertions.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.jupiter.api.Test;

import chess.model.ChessBoard;
import chess.model.Color;
import chess.model.Pawn;
import chess.model.Rook;
/**
 * Test class for RunGame class
 * @author Yingqin Fu, Steffen Häusler, Christoph Cohrs-Thiede from Gruppe 20 
 *
 */
public class RunGameTest {
	/**
	 * test for make chess move
	 */
	@Test
	public void testMakeChessMove() {
		
		ChessBoard board =new ChessBoard();
		
		
		boolean[][] potentialMove = new boolean[8][8];
		for (int j = 0; j < 8; j++){
			for (int k = 0; k < 8; k++){
				potentialMove[j][k] = false;
			}
		}
		StartGame.initWhiteFigures(board, potentialMove);
		//Pawn PW = new Pawn(4,6,"w", potentialMove);
		//board.setFigure(4, 6, PW);
		
		Input input = new Input();
		input.setInput("f2-f4");
		
		RunGame.makeChessMove(board, input);
		
		assertEquals("P", board.getFigure(5, 4).getName(), "correct move 1");
		assertEquals(null, board.getFigure(5, 6), "correct move 2");
	}
	
	
	/**
	 * test for checkRightTurn
	 */
	@Test
	public void testCheckRightturn() {
		
		ChessBoard board =new ChessBoard();
		
		
		boolean[][] potentialMove = new boolean[8][8];
		for (int j = 0; j < 8; j++){
			for (int k = 0; k < 8; k++){
				potentialMove[j][k] = false;
			}
		}
		Pawn PW = new Pawn(4,6,Color.white, potentialMove);
		Pawn PB = new Pawn(4,4,Color.black, potentialMove);
		board.setFigure(4, 6, PW);
		board.setFigure(4, 4, PB);
		Input input = new Input();
		//right turn
		input.setInput("e2-e4");
		
		assertTrue(RunGame.checkRightTurn(board, input), "correct move 3");
		
		//false turn
		input.setInput("e4-e5");
		assertFalse(RunGame.checkRightTurn(board, input), "false move");
	}
	
	/**
	 * test for make chess move
	 */
	@Test
	public void testMoveWithoutCheck() {
		
		ChessBoard board =new ChessBoard();
		
		
		boolean[][] potentialMove = new boolean[8][8];
		
		StartGame.initWhiteFigures(board, potentialMove);
		
		RunGame.boardSafe(board);
		Input input = new Input();
		input.setInput("e2-e4");
		
		
		RunGame.moveWithoutCheck(board, input);
		
		assertEquals("P", board.getFigure(4, 4).getName(), "correct move 4");
		assertEquals(null, board.getFigure(4, 6), "correct move 5");
	}
	
	
	
	/**
	 * test for run
	 */
	@Test
	public void testRun1() {
		//PrintStream save_out=System.out;
		final ByteArrayOutputStream out = new ByteArrayOutputStream();
		//final ByteArrayOutputStream out1 = new ByteArrayOutputStream();
		System.setOut(new PrintStream(out));
		//System.setOut(new PrintStream(out1));

		
		ChessBoard board =new ChessBoard();
		
		
		boolean[][] potentialMove = new boolean[8][8];
		
		
		
		Input input = new Input();
		input.setInput("e2-e3");
		StartGame.initWhiteFigures(board, potentialMove);
		RunGame.printInvalidOrEnd(board, input);
		
	    assertEquals("!Game Over\r\n", out.toString());
	    
	    
	}
	
	
	/**
	 * test for run
	 */
	@Test
	public void testRun2() {
		ChessBoard board =new ChessBoard();
		
		
		boolean[][] potentialMove = new boolean[8][8];
		
		StartGame.initWhiteFigures(board, potentialMove);
		StartGame.initBlackFigures(board, potentialMove);
		RunGame.boardSafe(board);
		Input input = new Input();
		input.setInput("e2-e3");
		
		RunGame.run(board,null,-1,input,true);
		
		assertEquals("P", board.getFigure(4, 5).getName(), "correct move 6");
		assertEquals(null, board.getFigure(4, 6), "correct move 7");
		
		ChessBoard board2 =new ChessBoard();
		StartGame.initBlackFigures(board2, potentialMove);
		input.setInput("e2-e2");
		RunGame.run(board,null,-1,input,true);
		
		assertEquals("r", board.getFigure(0, 0).getName());
		
	}
	
	/**
	 * test for beaten list
	 */
	@SuppressWarnings("static-access")
	@Test
	public void testBeatenList() {
		ChessBoard board =new ChessBoard();
		Input input = new Input();
		input.setInput("beaten");
		RunGame runGame=new RunGame();
		runGame.printBeatenList(board, input);
		 assertEquals("beaten",input.getInput());
		 
	}
	
	/**
	 * test for set AI input
	 */
	@Test
	public void testAIinput() {
		ChessBoard board =new ChessBoard();
	
		AdvancedAI aI = new AdvancedAI(Color.black,0);
		String mode ="pveb";
		Input cnslInput = new Input();
		cnslInput.setActivePlayer(2);
		
		RunGame.setAIinput(mode,cnslInput,board,aI,0);
		
		assertEquals(cnslInput.getInput(),aI.getHelper().getBestMove(board));
		 
		 AdvancedAI aI1 = new AdvancedAI(Color.white,0);
			String mode1 ="pvew";
			Input cnslInput1 = new Input();
			cnslInput.setActivePlayer(1);
			RunGame.setAIinput(mode1,cnslInput1,board,aI1,0);
			
			 assertEquals(cnslInput1.getInput(),aI1.getHelper().getBestMove(board));
	}
	
	/**
	 * test for set AI input
	 */
	@Test
	public void testAIinput2() {
		ChessBoard board =new ChessBoard();
		
		AdvancedAI aI = new AdvancedAI(Color.black,0);
		String mode ="pveb";
		Input cnslInput = new Input();
		cnslInput.setActivePlayer(2);
		RunGame.setAIinput(mode,cnslInput,board,aI,1);
		assertEquals(cnslInput.getInput(),aI.getBestMove(board));
		
		 AdvancedAI aI1 = new AdvancedAI(Color.white,0);
			String mode1 ="pvew";
			Input cnslInput1 = new Input();
			cnslInput.setActivePlayer(1);
			RunGame.setAIinput(mode1,cnslInput1,board,aI1,1);
			
			assertEquals(cnslInput1.getInput(),aI1.getBestMove(board));
		
	}
	/**
	 * test for return
	 */
	@Test
	public void testSetReturn() {
		ChessBoard board =new ChessBoard();
		boolean[][] potentialMove = new boolean[8][8];
		for (int j = 0; j < 8; j++){
			for (int k = 0; k < 8; k++){
				potentialMove[j][k] = false;
			}
		}
		Rook RW1 = new Rook(4,4,Color.white, potentialMove);
		board.setFigure(4, 4, RW1);
		RunGame.boardBackup(board);
		
		
		board.setFigureNull(4, 4);
		RunGame.returnBackup(board);
		
		assertEquals("R",board.getFigure(4, 4).getName());
		
		
	}
	
	/**
	 * test for copy board
	 */
	@Test
	public void testCopyBoard() {
		ChessBoard board=new ChessBoard();
		StartGame.initLineup(board);
		
		assertEquals(RunGame.boardcopy(board).getFigure(0, 0).getName(),board.getFigure(0, 0).getName());
		
	}
	

	
}