package chess.controller;

import static org.junit.jupiter.api.Assertions.*;



import org.junit.jupiter.api.Test;

import chess.model.ChessBoard;


/**
 * Test class for Start Game class
 * @author Yingqin Fu, Steffen Häusler, Christoph Cohrs-Thiede from Gruppe 20 
 *
 */
public class StartGameTest {
/**
 * Test for game start
 */
	@Test
	public void StartTest() {
		ChessBoard board =new ChessBoard();
		StartGame.initLineup(board);
		
		assertEquals("r", board.getFigure(0, 0).getName(), "null board");
		
		assertEquals(null, board.getFigure(4, 4), "null board");
		
	}
	

}
