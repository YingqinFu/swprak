package chess.model;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;


/**
 * Test class for ChessBoard class
 * @author Yingqin Fu, Steffen Häusler, Christoph Cohrs-Thiede from Gruppe 20 
 *
 */
public class ChessBoardTest {

	
	/**
	* Test For figures on chess board
	*/	
	@Test
	public void testFigure () {
	
		boolean[][] potentialMove = new boolean[8][8];
		for (int j = 0; j < 8; j++){
			for (int k = 0; k < 8; k++){
				potentialMove[j][k] = false;
			}
		}
		ChessBoard board= new ChessBoard();
		Bishop BW = new Bishop(3,2,Color.white, potentialMove);
		board.setFigure(3, 2, BW);
		
		assertEquals("B", board.getFigure(3, 2).getName(), "normal figure");
		
		board.setFigureNull(3, 2);
		
		assertEquals(null, board.getFigure(3, 2), "null figure");

	}
	
	/**
	 * test for gui method
	 */
	@Test
	public void testGui1() {
		ChessBoard board= new ChessBoard();
		
		board.setGuiMode(false);
		assertFalse(board.isGuiMode(), "correct mode");
		
		board.setPvp(false);
		assertFalse(board.isPvp(), "correct pvp");
		
		board.setLastMoveValid(false);
		assertFalse(board.isLastMoveValid(), "correct last move");
		
		board.setReselect(false);
		assertFalse(board.isReselect(), "correct reselect");
		
		
		
	}
	
	/**
	 * test for gui method
	 */
	@Test
	public void testGui2() {
		ChessBoard board= new ChessBoard();
		
		
		
		board.setSelectedField(null);
		assertEquals(null,board.getSelectedField(), "correct selected field");
		
		board.setShowInCheck(false);
		assertFalse(board.isShowInCheck(), "correct check");
		
		board.setShowMoves(false);
		assertFalse(board.isShowMoves(), "correct move");
		
		board.setTurnField(false);
		assertFalse(board.isTurnField(), "correct turn");
		
		
	}
	
	
	/**
	 * test for toUTF8
	 */
	@Test
	public void testUTF8() {
		String fig ="p";
		ChessBoard board= new ChessBoard();
		
		
		assertEquals("♟",board.toUTF8(fig), "correct selected field");
		
	
	}
	
	/**
	 * test for toUTF8
	 */
	@Test
	public void testCopyFigure() {
		ChessBoard board= new ChessBoard();
		
		Figure [][] copy=new Figure[8][8];
		Figure [][] figures=new Figure[8][8];
		ChessBoard board1= new ChessBoard(figures);
		board1.copyFigure(figures, copy);
		board.copyFigure(figures,copy);
		
		for (int j = 0; j < 8; j++){
			for (int k = 0; k < 8; k++){
				assertEquals(copy[j][k],figures[j][k], "correct copy");
			}
		}
	}
	
	
}

	
	
	




	



