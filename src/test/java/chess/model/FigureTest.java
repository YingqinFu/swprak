package chess.model;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import chess.controller.Input;
import chess.controller.RunGame;
import chess.controller.StartGame;
/**
 * Test class for figure
 * @author Yingqin Fu
 *
 */
public class FigureTest {
/**
 * test for hasMoved
 */
	@Test
	public void hasMovedTest() {
		ChessBoard board =new ChessBoard();
		StartGame.initLineup(board);
		
		Input input =new Input();
		input.setInput("e2-e3");
		
		RunGame.makeChessMove(board, input);
		assertTrue(board.getFigure(4, 5).hasMoved(board));
	}
	
	
	/**
	 * test for getter
	 */
	@Test
	public void GetTest() {
		boolean[][] potentialMove = new boolean[8][8];
		Figure figure=new Figure(0, 0, Color.white, potentialMove);
		figure.setPotentialMove(0, 0, null);
		assertEquals("", figure.getUnicode());
		
		
		assertEquals(null, figure.getName());
		
		assertEquals(0, figure.getPoints());
		
		
		
	}

}
