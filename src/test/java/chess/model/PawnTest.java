package chess.model;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import chess.controller.Input;
/**
 * Test class for pawn class
 * @author Yingqin Fu, Steffen Häusler, Christoph Cohrs-Thiede from Gruppe 20 
 *
 */
public class PawnTest {

	/**
	* Test For pawn name
	*/	
	@Test
	public void testPawnName () {
		boolean[][] potentialMove = new boolean[8][8];
		for (int j = 0; j < 8; j++){
			for (int k = 0; k < 8; k++){
				potentialMove[j][k] = false;
				
			}
		}
		Pawn PW = new Pawn(3,2,Color.white, potentialMove);
		Pawn PB = new Pawn(1,2,Color.black, potentialMove);
		
		assertEquals("P", PW.getName(), "Color white");
		assertEquals("p", PB.getName(), "Color black");
		

	}
	
	
	/**
	* Test For pawn position
	*/	
	@Test
	public void testPawnPos () {
		boolean[][] potentialMove = new boolean[8][8];
		for (int j = 0; j < 8; j++){
			for (int k = 0; k < 8; k++){
				potentialMove[j][k] = false;
			}
		}
		Pawn PW = new Pawn(3,2,Color.white, potentialMove);
		
		
		assertEquals(3, PW.getX(), "Correct x");
		assertEquals(2, PW.getY(), "Correct y");
		
	}
	
	
	/**
	* Test For pawn color
	*/	
	@Test
	public void testPawnColor () {
		boolean[][] potentialMove = new boolean[8][8];
		for (int j = 0; j < 8; j++){
			for (int k = 0; k < 8; k++){
				potentialMove[j][k] = false;
			}
		}
		Pawn PW = new Pawn(3,2,Color.white, potentialMove);
		Pawn PB = new Pawn(3,2,Color.black, potentialMove);
		
		assertEquals(Color.white, PW.getColor(), "Correct white color");
		assertEquals(Color.black, PB.getColor(), "Correct black color");
		
	

	}
	
	
	
	/**
	* Test For pawn potential move
	*/	
	@Test
	public void testPawnPotentialMove1 () {
		ChessBoard board= new ChessBoard();
		
		boolean[][] potentialMove = new boolean[8][8];
		for (int j = 0; j < 8; j++){
			for (int k = 0; k < 8; k++){
				potentialMove[j][k] = false;
			}
		}
		Pawn PW1 = new Pawn(4,6,Color.white, potentialMove);
		
		Knight KB = new Knight(5,5,Color.black, potentialMove);
		board.setFigure(4, 6, PW1);
		board.setFigure(5, 5, KB);
		PW1.setPotentialMove(4, 6, board);
		
		
		//the normal move. Pawn can move one or two squares,when it is at the initial position
		assertTrue(PW1.getPotentialMove()[4][4], "correct potential move 1");
		
		assertTrue(PW1.getPotentialMove()[4][5], "correct potential move 2");
		
		//capture
		assertTrue(PW1.getPotentialMove()[5][5], "capture");
		
	}
	
	
	
	/**
	* Test For pawn potential move
	*/	
	@Test
	public void testPawnPotentialMove2 () {
		ChessBoard board= new ChessBoard();
		
		boolean[][] potentialMove = new boolean[8][8];
		for (int j = 0; j < 8; j++){
			for (int k = 0; k < 8; k++){
				potentialMove[j][k] = false;
			}
		}
		Pawn PW1 = new Pawn(4,5,Color.white, potentialMove);
		
		Knight KB = new Knight(5,4,Color.black, potentialMove);
		board.setFigure(4, 5, PW1);
		board.setFigure(5, 4, KB);
		PW1.setPotentialMove(4, 5, board);
		
		
		//the normal move. Pawn can move one square ,when it is not at the initial position
		assertTrue(PW1.getPotentialMove()[4][4], "correct potential move 3");
		
		assertFalse(PW1.getPotentialMove()[4][3], "incorrect potential move");
		
		//capture
		assertTrue(PW1.getPotentialMove()[5][4], "capture");
		
		
	}
	
	
	
	/**
	* Test For white pawn promotion
	*/	
	@Test
	public void testWhitePawnPromotion () {
		ChessBoard board= new ChessBoard();
		
		boolean[][] potentialMove = new boolean[8][8];
		for (int j = 0; j < 8; j++){
			for (int k = 0; k < 8; k++){
				potentialMove[j][k] = false;
			}
		}
		Pawn PW1 = new Pawn(4,1,Color.white, potentialMove);
		
		
		board.setFigure(4, 1, PW1);
		
		PW1.setPotentialMove(4, 1, board);
		
		Input input=new Input();
		
	
		input.setInput("e7-e8");
		PW1.whitePromotion(input, board);
		assertEquals("Q", board.getFigure(4, 0).getName(), "correct promotion to queen 1");
		
		input.setInput("e7-e8R");
		PW1.whitePromotion(input, board);
		assertEquals("R", board.getFigure(4, 0).getName(), "correct promotion to rook ");
		
		input.setInput("e7-e8B");
		PW1.whitePromotion(input, board);
		assertEquals("B", board.getFigure(4, 0).getName(), "correct promotion to bishop");
		
		input.setInput("e7-e8N");
		PW1.whitePromotion(input, board);
		assertEquals("N", board.getFigure(4, 0).getName(), "correct promotion to knight");
		
		input.setInput("e7-e8Q");
		PW1.whitePromotion(input, board);
		assertEquals("Q", board.getFigure(4, 0).getName(), "correct promotion to queen 2");
		
	
		
	}
	
	/**
	* Test For white pawn promotion
	*/	
	@Test
	public void testBlackPawnPromotion () {
		ChessBoard board= new ChessBoard();
		
		boolean[][] potentialMove = new boolean[8][8];
		for (int j = 0; j < 8; j++){
			for (int k = 0; k < 8; k++){
				potentialMove[j][k] = false;
			}
		}
		Pawn PB1 = new Pawn(4,6,Color.black, potentialMove);
		
		
		board.setFigure(4, 6, PB1);
		
		PB1.setPotentialMove(4, 6, board);
		
		Input input=new Input();
		
	
		input.setInput("e2-e1");
		PB1.blackPromotion(input, board);
		assertEquals("q", board.getFigure(4, 7).getName(), "correct promotion to queen 1");
		
		input.setInput("e2-e1R");
		PB1.blackPromotion(input, board);
		assertEquals("r", board.getFigure(4, 7).getName(), "correct promotion to rook ");
		
		input.setInput("e2-e1B");
		PB1.blackPromotion(input, board);
		assertEquals("b", board.getFigure(4, 7).getName(), "correct promotion to bishop");
		
		input.setInput("e2-e1N");
		PB1.blackPromotion(input, board);
		assertEquals("n", board.getFigure(4, 7).getName(), "correct promotion to knight");
		
		input.setInput("e2-e1Q");
		PB1.blackPromotion(input, board);
		assertEquals("q", board.getFigure(4, 7).getName(), "correct promotion to queen 2");
		
	
		
	}
	
	
	/**
	* Test For white pawn en passant
	*/	
	@Test
	public void testWhitePawnEnPassant1 () {
		ChessBoard board= new ChessBoard();
		
		boolean[][] potentialMove = new boolean[8][8];
		for (int j = 0; j < 8; j++){
			for (int k = 0; k < 8; k++){
				potentialMove[j][k] = false;
			}
		}
		Pawn PW = new Pawn(4,3,Color.white, potentialMove);
		Pawn PB = new Pawn(5,3,Color.black,potentialMove);
		
		board.setFigure(4, 3, PW);
		board.setFigure(5, 3, PB);
		
		board.getMovedList().add("f7-f5");
		PW.setPotentialMove(4, 1, board);
		
		Input input=new Input();
		input.setInput("e5-f6");
		
		PW.whiteEnPassant(input, board);
		
		assertTrue(PW.getPotentialMove()[5][2], "correct en passant 1");
		
		
	}
	
	/**
	* Test For white pawn en passant
	*/	
	@Test
	public void testWhitePawnEnPassant2 () {
		ChessBoard board= new ChessBoard();
		
		boolean[][] potentialMove = new boolean[8][8];
		for (int j = 0; j < 8; j++){
			for (int k = 0; k < 8; k++){
				potentialMove[j][k] = false;
			}
		}
		Pawn PW = new Pawn(4,3,Color.white, potentialMove);
		Pawn PB = new Pawn(3,3,Color.black,potentialMove);
		
		board.setFigure(4, 3, PW);
		board.setFigure(3, 3, PB);
		
		board.getMovedList().add("d7-d5");
		//PW.setPotentialMove(4, 1, board);
		
		Input input=new Input();
		input.setInput("e5-d6");
		
		PW.whiteEnPassant(input, board);
		
		assertTrue(PW.getPotentialMove()[3][2], "correct en passant 2");
		
		
	}
	
	
	
	/**
	* Test For black pawn en passant
	*/	
	@Test
	public void testBlackPawnEnPassant1 () {
		ChessBoard board= new ChessBoard();
		
		boolean[][] potentialMove = new boolean[8][8];
		for (int j = 0; j < 8; j++){
			for (int k = 0; k < 8; k++){
				potentialMove[j][k] = false;
			}
		}
		Pawn PB = new Pawn(4,4,Color.black, potentialMove);
		Pawn PW = new Pawn(5,4,Color.white,potentialMove);
		
		board.setFigure(4, 4, PB);
		board.setFigure(5, 4, PW);
		
		board.getMovedList().add("f2-f4");
		PB.setPotentialMove(4, 1, board);
		
		Input input=new Input();
		input.setInput("e4-f5");
		
		PB.blackEnPassant(input, board);
		
		assertFalse(PW.getPotentialMove()[5][5], "correct en passant 3");
		
		
	}
	
	
	/**
	* Test For black pawn en passant
	*/	
	@Test
	public void testBlackPawnEnPassant2 () {
		ChessBoard board= new ChessBoard();
		
		boolean[][] potentialMove = new boolean[8][8];
		for (int j = 0; j < 8; j++){
			for (int k = 0; k < 8; k++){
				potentialMove[j][k] = false;
			}
		}
		Pawn PB = new Pawn(4,4,Color.black,potentialMove);
		Pawn PW = new Pawn(3,4,Color.white,potentialMove);
		
		board.setFigure(4, 4, PB);
		board.setFigure(3, 4, PW);
		
		board.getMovedList().add("d2-d4");
		
		
		Input input=new Input();
		input.setInput("e4-d5");
		
		PB.blackEnPassant(input, board);
		
		assertFalse(PW.getPotentialMove()[3][5], "correct en passant 4");
		
		
	}
	
	/**
	* Test For pawn potential move
	*/	
	@Test
	public void testBlackPawnPotentialMove1 () {
		ChessBoard board= new ChessBoard();
		
		boolean[][] potentialMove = new boolean[8][8];
		for (int j = 0; j < 8; j++){
			for (int k = 0; k < 8; k++){
				potentialMove[j][k] = false;
			}
		}
		Pawn PB1 = new Pawn(4,1,Color.black, potentialMove);
		
		Knight KW = new Knight(5,2,Color.white, potentialMove);
		board.setFigure(4, 1, PB1);
		board.setFigure(5, 2, KW);
		PB1.setPotentialMove(4, 1, board);
		
		
		//the normal move. Pawn can move one or two squares,when it is at the initial position
		assertTrue(PB1.getPotentialMove()[4][2], "correct potential move 1");
		
		assertTrue(PB1.getPotentialMove()[4][3], "correct potential move 2");
		
		//capture
		assertTrue(PB1.getPotentialMove()[5][2], "capture");
		
	}
	
	/**
	* Test For pawn Unicode
	*/	
	@Test
	public void testPawnUniCode () {
		boolean[][] potentialMove = new boolean[8][8];
		for (int j = 0; j < 8; j++){
			for (int k = 0; k < 8; k++){
				potentialMove[j][k] = false;
			}
		}
		Pawn RW = new Pawn(3,2,Color.white, potentialMove);
		Pawn RB = new Pawn(1,2,Color.black, potentialMove);
		
		
		
		assertEquals("♙", RW.getUnicode(), "white pawn");
		assertEquals("♟", RB.getUnicode(), "black pawn");
		
		
		
		
	}
	
	

}
