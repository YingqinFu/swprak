package chess.model;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import chess.controller.Input;
/**
 * Test class for King class
 * @author Yingqin Fu, Steffen Häusler, Christoph Cohrs-Thiede from Gruppe 20 
 *
 */
public class KingTest {

	/**
	* Test For king name
	*/	
	@Test
	public void testKingName () {
		boolean[][] potentialMove = new boolean[8][8];
		for (int j = 0; j < 8; j++){
			for (int k = 0; k < 8; k++){
				potentialMove[j][k] = false;
				
			}
		}
		King KW = new King(3,2,Color.white, potentialMove);
		King KB = new King(1,2,Color.black, potentialMove);
		
		assertEquals("K", KW.getName(), "Color white");
		assertEquals("k", KB.getName(), "Color black");
		

	}
	
	/**
	* Test For King position
	*/	
	@Test
	public void testKingPos () {
		boolean[][] potentialMove = new boolean[8][8];
		for (int j = 0; j < 8; j++){
			for (int k = 0; k < 8; k++){
				potentialMove[j][k] = false;
			}
		}
		King KW = new King(3,2,Color.white, potentialMove);
		
		
		assertEquals(3, KW.getX(), "Correct x");
		assertEquals(2, KW.getY(), "Correct y");
		
	}
	
	
	

	/**
	* Test For King color
	*/	
	@Test
	public void testKingColor () {
		boolean[][] potentialMove = new boolean[8][8];
		for (int j = 0; j < 8; j++){
			for (int k = 0; k < 8; k++){
				potentialMove[j][k] = false;
			}
		}
		King KW = new King(3,2,Color.white, potentialMove);
		King KB = new King(3,2,Color.black, potentialMove);
		
		assertEquals(Color.white, KW.getColor(), "Correct white color");
		assertEquals(Color.black, KB.getColor(), "Correct black color");
		
	

	}
	
	
	

	/**
	* Test For king potential move
	*/	
	@Test
	public void testKingPotentialMove () {
		ChessBoard board= new ChessBoard();
		
		boolean[][] potentialMove = new boolean[8][8];
		for (int j = 0; j < 8; j++){
			for (int k = 0; k < 8; k++){
				potentialMove[j][k] = false;
			}
		}
		King KW1 = new King(4,4,Color.white, potentialMove);
		King KW2 = new King(4,3,Color.white, potentialMove);
		King KB = new King(5,5,Color.black, potentialMove);
		board.setFigure(4, 4, KW1);
		board.setFigure(4, 3, KW2);
		board.setFigure(5, 5, KB);
		KW1.setPotentialMove(4, 4, board);
		
		
		
		
		//the normal move
		assertTrue(KW1.getPotentialMove()[5][3], "correct potential move 1");
		
		assertTrue(KW1.getPotentialMove()[3][3], "correct potential move 2");
		
		
		
		//Knight can not move in any other way
		assertFalse(KW1.getPotentialMove()[4][2], "incorrect potential move 1");
		
		//can not capture the own side figures
		assertFalse(KW1.getPotentialMove()[4][3], "correct potential move 3");
		
		//capture
		assertTrue(KW1.getPotentialMove()[5][5], "capture" );
		
	

	}
	
	
	/**
	* Test For white king castling
	*/	
	@Test
	public void testWhiteKingCastling() {
		
		ChessBoard board= new ChessBoard();
		
		boolean[][] potentialMove = new boolean[8][8];
		for (int j = 0; j < 8; j++){
			for (int k = 0; k < 8; k++){
				potentialMove[j][k] = false;
			}
		}
		King KW = new King(4,7,Color.white, potentialMove);
		Rook RW1 = new Rook(7,7,Color.white, potentialMove);
		Rook RW2 =new Rook(0,7,Color.white, potentialMove);
		board.setFigure(4, 7, KW);
		board.setFigure(7, 7, RW1);
		board.setFigure(0, 7, RW2);
		
		KW.setPotentialMove(4, 4, board);
		
		Input input=new Input();
		
		//short castling
		input.setInput("e1-g1");
		
		KW.whiteCastling(input, board);
		
		
		assertTrue(KW.getPotentialMove()[6][7], "correct potential move 1");
		
		assertEquals("R", board.getFigure(5, 7).getName(), "correct short castling");
		
		//long castling
		
		input.setInput("e1-c1");
		KW.whiteCastling(input, board);
		
		assertTrue(KW.getPotentialMove()[2][7], "correct potential move 2");
		
		assertEquals("R", board.getFigure(3, 7).getName(), "correct long castling");
		
		
		
	}
	
	

	/**
	* Test For white king castling
	*/	
	@Test
	public void testBlackKingCastling() {
		
		ChessBoard board= new ChessBoard();
		
		boolean[][] potentialMove = new boolean[8][8];
		for (int j = 0; j < 8; j++){
			for (int k = 0; k < 8; k++){
				potentialMove[j][k] = false;
			}
		}
		King KB = new King(4,0,Color.black, potentialMove);
		Rook RB1 = new Rook(7,0,Color.black, potentialMove);
		Rook RB2 =new Rook(0,0,Color.black, potentialMove);
		board.setFigure(4, 0, KB);
		board.setFigure(7, 0, RB1);
		board.setFigure(0, 0, RB2);
		
		KB.setPotentialMove(4, 0, board);
		
		Input input=new Input();
		
		//short castling
		input.setInput("e8-g8");
		
		KB.blackCastling(input, board);
		
		
		assertTrue(KB.getPotentialMove()[6][0], "correct potential move 1");
		
		assertEquals("r", board.getFigure(5, 0).getName(), "correct short castling");
		
		//long castling
		
		input.setInput("e8-c8");
		KB.blackCastling(input, board);
		
		assertTrue(KB.getPotentialMove()[2][0], "correct long castling");
		
		assertEquals("r", board.getFigure(3, 0).getName(), "correct long castling");
		
		
		
	}
	
	

	/**
	* Test For king Unicode
	*/	
	@Test
	public void testKingUniCode () {
		boolean[][] potentialMove = new boolean[8][8];
		for (int j = 0; j < 8; j++){
			for (int k = 0; k < 8; k++){
				potentialMove[j][k] = false;
			}
		}
		King RW = new King(3,2,Color.white, potentialMove);
		King RB = new King(1,2,Color.black, potentialMove);
		
		
		assertEquals("♔", RW.getUnicode(), "white king");
		assertEquals("♚", RB.getUnicode(), "black king");
		
		
		
		
	}
	
}
