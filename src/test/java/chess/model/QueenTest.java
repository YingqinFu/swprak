package chess.model;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
/**
 * Test class for Queen class
 * @author Yingqin Fu, Steffen Häusler, Christoph Cohrs-Thiede from Gruppe 20 
 *
 */
public class QueenTest {

	/**
	* Test For queen name
	*/	
	@Test
	public void testQueenName () {
		boolean[][] potentialMove = new boolean[8][8];
		for (int j = 0; j < 8; j++){
			for (int k = 0; k < 8; k++){
				potentialMove[j][k] = false;
			}
		}
		Queen QW = new Queen(3,2,Color.white, potentialMove);
		Queen QB = new Queen(1,2,Color.black, potentialMove);
		
		assertEquals("Q", QW.getName(), "Color white");
		assertEquals("q", QB.getName(), "Color black");
		

	}
	
	

	/**
	* Test For queen position
	*/	
	@Test
	public void testQueenPos () {
		boolean[][] potentialMove = new boolean[8][8];
		for (int j = 0; j < 8; j++){
			for (int k = 0; k < 8; k++){
				potentialMove[j][k] = false;
			}
		}
		Queen QW = new Queen(3,2,Color.white, potentialMove);
		
		
		assertEquals(3, QW.getX(), "Correct x");
		assertEquals(2, QW.getY(), "Correct y");
		
	}
	
	

	/**
	* Test For queen color
	*/	
	@Test
	public void testQueenColor () {
		boolean[][] potentialMove = new boolean[8][8];
		for (int j = 0; j < 8; j++){
			for (int k = 0; k < 8; k++){
				potentialMove[j][k] = false;
			}
		}
		Queen QW = new Queen(3,2,Color.white, potentialMove);
		Queen QB = new Queen(3,2,Color.black, potentialMove);
		
		assertEquals(Color.white, QW.getColor(), "Correct white color");
		assertEquals(Color.black, QB.getColor(), "Correct black color");
		
	

	}
	
	
	
	/**
	* Test For queen potential move
	*/	
	@Test
	public void testQueenPotentialMove () {
		
		ChessBoard board= new ChessBoard();
		
		boolean[][] potentialMove = new boolean[8][8];
		for (int j = 0; j < 8; j++){
			for (int k = 0; k < 8; k++){
				potentialMove[j][k] = false;
			}
		}
		Queen QW1 = new Queen(4,4,Color.white, potentialMove);
		Queen QW2 = new Queen(5,4,Color.white, potentialMove);
		Queen QB = new Queen(4,2,Color.black, potentialMove);
		
		board.setFigure(4, 4, QW1);
		board.setFigure(5, 4, QW2);
		board.setFigure(4, 2, QB);
		QW1.setPotentialMove(4, 4, board);
		
		 //move straight
		assertTrue(QW1.getPotentialMove()[4][3], "correct potential move 1");
		
		assertTrue(QW1.getPotentialMove()[3][4], "correct potential move 2");
		
		
		
		//can not across other
		assertFalse(QW1.getPotentialMove()[5][4], "false potential move");
		
		//capture
		assertTrue(QW1.getPotentialMove()[4][2], "capture");
		
		
	

	}
	

	/**
	* Test For Queen Unicode
	*/	
	@Test
	public void testQueenUniCode () {
		boolean[][] potentialMove = new boolean[8][8];
		for (int j = 0; j < 8; j++){
			for (int k = 0; k < 8; k++){
				potentialMove[j][k] = false;
			}
		}
		Queen RW = new Queen(3,2,Color.white, potentialMove);
		Queen RB = new Queen(1,2,Color.black, potentialMove);
		
		
		assertEquals("♕", RW.getUnicode(), "white queen");
		assertEquals("♛", RB.getUnicode(), "black queen");
	

		
		
		
	}
	

}
