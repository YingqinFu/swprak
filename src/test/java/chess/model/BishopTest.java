package chess.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;




/**
 * Test class for Bishop class
 * @author Yingqin Fu, Steffen Häusler, Christoph Cohrs-Thiede from Gruppe 20 
 *
 */
public class BishopTest {
	

	/**
	* Test For Bishop name
	*/	
	@Test
	public void testBishopName () {
		boolean[][] potentialMove = new boolean[8][8];
		for (int j = 0; j < 8; j++){
			for (int k = 0; k < 8; k++){
				potentialMove[j][k] = false;
			}
		}
		Bishop BW = new Bishop(3,2,Color.white, potentialMove);
		Bishop BB = new Bishop(1,2,Color.black, potentialMove);
		
		
		assertEquals("B", BW.getName(), "Color white");
		assertEquals("b", BB.getName(), "Color black");
		
		

	}
	
	/**
	* Test For Bishop position
	*/	
	@Test
	public void testBishopPos () {
		boolean[][] potentialMove = new boolean[8][8];
		for (int j = 0; j < 8; j++){
			for (int k = 0; k < 8; k++){
				potentialMove[j][k] = false;
			}
		}
		Bishop BW = new Bishop(3,2,Color.white, potentialMove);
		
		
		assertEquals(3, BW.getX(), "Correct x");
		assertEquals(2, BW.getY(), "Correct y");
		
	

	}
	
	
	/**
	* Test For Bishop color
	*/	
	@Test
	public void testBishopColor () {
		boolean[][] potentialMove = new boolean[8][8];
		for (int j = 0; j < 8; j++){
			for (int k = 0; k < 8; k++){
				potentialMove[j][k] = false;
			}
		}
		Bishop BW = new Bishop(3,2,Color.white, potentialMove);
		Bishop BB = new Bishop(3,2,Color.black, potentialMove);
		
		assertEquals(Color.white, BW.getColor(), "Correct white color");
		assertEquals(Color.black, BB.getColor(), "Correct black color");
		
	

	}
	
	/**
	* Test For Bishop potential move
	*/	
	@Test
	public void testBishopPotentialMove1 () {
		ChessBoard board= new ChessBoard();
		
		boolean[][] potentialMove = new boolean[8][8];
		for (int j = 0; j < 8; j++){
			for (int k = 0; k < 8; k++){
				potentialMove[j][k] = false;
			}
		}
		Bishop BW1 = new Bishop(3,2,Color.white, potentialMove);
		Bishop BW2 = new Bishop(5,4,Color.white, potentialMove);
		Bishop BB = new Bishop(5,0,Color.black, potentialMove);
		
		BW1.setPotentialMove(3, 2, board);
		BW2.setPotentialMove(3, 2, board);
		BB.setPotentialMove(3, 2, board);
		
		
		//right top
		assertTrue(BW1.getPotentialMove()[4][1], "correct potential move 1");
		//left top
		assertTrue(BW1.getPotentialMove()[2][1], "correct potential move 2");
		
		
		//capture
		assertTrue(BW1.getPotentialMove()[5][0], "capture");
		
		//Bishop can not move straight
		assertFalse(BW1.getPotentialMove()[3][1], "false potential move 1");
		
		//can not across other
		assertFalse(BW1.getPotentialMove()[6][4], "false potential move 2");
		
	}
	
	
	/**
	* Test For Bishop potential move
	*/	
	@Test
	public void testBishopPotentialMove2 () {
		ChessBoard board= new ChessBoard();
		
		boolean[][] potentialMove = new boolean[8][8];
		for (int j = 0; j < 8; j++){
			for (int k = 0; k < 8; k++){
				potentialMove[j][k] = false;
			}
		}
		Bishop BW1 = new Bishop(4,4,Color.white, potentialMove);
		
		Bishop BB1 = new Bishop(3,3,Color.black, potentialMove);
		Bishop BB2 = new Bishop(5,3,Color.black, potentialMove);
		Bishop BB3 = new Bishop(3,5,Color.black, potentialMove);
		Bishop BB4 = new Bishop(5,5,Color.black, potentialMove);
		
		board.setFigure(4, 4, BW1);
		board.setFigure(3, 3, BB1);
		board.setFigure(5, 3, BB2);
		board.setFigure(3, 5, BB3);
		board.setFigure(5, 5, BB4);
		
		BW1.setPotentialMove(4, 4, board);
		
		
		
		
		assertTrue(BW1.getPotentialMove()[3][3], "correct potential move 1");
		
		assertTrue(BW1.getPotentialMove()[5][3], "correct potential move 2");
		
		
		
		assertTrue(BW1.getPotentialMove()[3][5], "correct potential move 3");
		
		
		assertTrue(BW1.getPotentialMove()[5][5], "correct potential move 4");
		
		
		
		
	}
	/**
	* Test For bishop Unicode
	*/	
	@Test
	public void testBishopUniCode () {
		boolean[][] potentialMove = new boolean[8][8];
		for (int j = 0; j < 8; j++){
			for (int k = 0; k < 8; k++){
				potentialMove[j][k] = false;
			}
		}
		Bishop RW = new Bishop(3,2,Color.white, potentialMove);
		Bishop RB = new Bishop(1,2,Color.black, potentialMove);
		
		
		assertEquals("♗", RW.getUnicode(), "white king");
		assertEquals("♝", RB.getUnicode(), "black king");
		
		
		
		
	}
	

}

