package chess.model;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
/**
 * Test class for Rook class
 * @author Yingqin Fu, Steffen Häusler, Christoph Cohrs-Thiede from Gruppe 20 
 *
 */
public class RookTest {
	/**
	* Test For rook name
	*/	
	@Test
	public void testRookName () {
		boolean[][] potentialMove = new boolean[8][8];
		for (int j = 0; j < 8; j++){
			for (int k = 0; k < 8; k++){
				potentialMove[j][k] = false;
			}
		}
		Rook RW = new Rook(3,2,Color.white, potentialMove);
		Rook RB = new Rook(1,2,Color.black, potentialMove);
		//Rook RQ = new Rook(3,2,Color.,potentialMove);
		assertEquals("R", RW.getName(), "Color white");
		assertEquals("r", RB.getName(), "Color black");
		//assertEquals(null, RQ.getName(), "false color");

	}
	
	/**
	* Test For rook position
	*/	
	@Test
	public void testRookPos () {
		boolean[][] potentialMove = new boolean[8][8];
		for (int j = 0; j < 8; j++){
			for (int k = 0; k < 8; k++){
				potentialMove[j][k] = false;
			}
		}
		Rook RW = new Rook(3,2,Color.white, potentialMove);
		
		
		assertEquals(3, RW.getX(), "Correct x");
		assertEquals(2, RW.getY(), "Correct y");
		
	

	}
	
	
	
	/**
	* Test For rook color
	*/	
	@Test
	public void testRookColor () {
		boolean[][] potentialMove = new boolean[8][8];
		for (int j = 0; j < 8; j++){
			for (int k = 0; k < 8; k++){
				potentialMove[j][k] = false;
			}
		}
		Rook RW = new Rook(3,2,Color.white, potentialMove);
		Rook RB = new Rook(3,2,Color.black, potentialMove);
		
		assertEquals(Color.white, RW.getColor(), "Correct white color");
		assertEquals(Color.black, RB.getColor(), "Correct black color");
		
	

	}
	
	
	
	/**
	* Test For rook potential move
	*/	
	@Test
	public void testRookPotentialMove () {
		
		ChessBoard board= new ChessBoard();
		
		boolean[][] potentialMove = new boolean[8][8];
		for (int j = 0; j < 8; j++){
			for (int k = 0; k < 8; k++){
				potentialMove[j][k] = false;
			}
		}
		Rook RW1 = new Rook(4,4,Color.white, potentialMove);
		Rook RW2 = new Rook(5,4,Color.white, potentialMove);
		Rook RB = new Rook(4,2,Color.black, potentialMove);
		
		board.setFigure(4, 4, RW1);
		board.setFigure(5, 4, RW2);
		board.setFigure(4, 2, RB);
		RW1.setPotentialMove(4, 4, board);
		
		 //move straight
		assertTrue(RW1.getPotentialMove()[4][3], "correct potential move 1");
		
		assertTrue(RW1.getPotentialMove()[3][4], "correct potential move 2");
		
		assertTrue(RW1.getPotentialMove()[4][5], "correct potential move 3");
		
		//can not across other
		assertFalse(RW1.getPotentialMove()[5][4], "false potential move");
		
		//capture
		assertTrue(RW1.getPotentialMove()[4][2], "capture");
		
		
	

	}
	
	
	/**
	* Test For Bishop potential move
	*/	
	@Test
	public void testRookPotentialMove2 () {
		ChessBoard board= new ChessBoard();
		
		boolean[][] potentialMove = new boolean[8][8];
		for (int j = 0; j < 8; j++){
			for (int k = 0; k < 8; k++){
				potentialMove[j][k] = false;
			}
		}
		Rook RW1 = new Rook(4,4,Color.white, potentialMove);
		
		Bishop BB1 = new Bishop(4,3,Color.black, potentialMove);
		Bishop BB2 = new Bishop(4,5,Color.black, potentialMove);
		Bishop BB3 = new Bishop(3,4,Color.black, potentialMove);
		Bishop BB4 = new Bishop(5,4,Color.black, potentialMove);
		
		board.setFigure(4, 4, RW1);
		board.setFigure(4, 3, BB1);
		board.setFigure(4, 5, BB2);
		board.setFigure(3, 4, BB3);
		board.setFigure(5, 4, BB4);
		
		RW1.setPotentialMove(4, 4, board);
		
		
		
		
		assertTrue(RW1.getPotentialMove()[4][3], "correct potential move 1");
		
		assertTrue(RW1.getPotentialMove()[4][5], "correct potential move 2");
		
		
		
		assertTrue(RW1.getPotentialMove()[3][4], "correct potential move 3");
		
		
		assertTrue(RW1.getPotentialMove()[5][4], "correct potential move 4");
		
		
		
		
	}
	
	
	/**
	* Test For rook Unicode
	*/	
	@Test
	public void testRookUniCode () {
		boolean[][] potentialMove = new boolean[8][8];
		for (int j = 0; j < 8; j++){
			for (int k = 0; k < 8; k++){
				potentialMove[j][k] = false;
			}
		}
		Rook RW = new Rook(3,2,Color.white, potentialMove);
		Rook RB = new Rook(1,2,Color.black, potentialMove);
		//Rook KB = new Rook(1,2,"Q", potentialMove);
		
		assertEquals("♖", RW.getUnicode(), "white rook");
		assertEquals("♜", RB.getUnicode(), "black rook");
		//assertEquals(null, KB.getUnicode(), "NULL");

		
		
		
	}
	
	
}
