package chess.model;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;


/**
 * Test class for Knight class
 * @author Yingqin Fu, Steffen Häusler, Christoph Cohrs-Thiede from Gruppe 20 
 *
 */
public class KnightTest {

	/**
	* Test For knight name
	*/	
	@Test
	public void testKnightName () {
		boolean[][] potentialMove = new boolean[8][8];
		for (int j = 0; j < 8; j++){
			for (int k = 0; k < 8; k++){
				potentialMove[j][k] = false;
				
			}
		}
		Knight NW = new Knight(3,2,Color.white, potentialMove);
		Knight NB = new Knight(1,2,Color.black, potentialMove);
		
		assertEquals("N", NW.getName(), "Color white");
		assertEquals("n", NB.getName(), "Color black");
		

	}
	
	/**
	* Test For Knight position
	*/	
	@Test
	public void testKnightPos () {
		boolean[][] potentialMove = new boolean[8][8];
		for (int j = 0; j < 8; j++){
			for (int k = 0; k < 8; k++){
				potentialMove[j][k] = false;
			}
		}
		Knight NW = new Knight(3,2,Color.white, potentialMove);
		
		
		assertEquals(3, NW.getX(), "Correct x");
		assertEquals(2, NW.getY(), "Correct y");
		
	}
	
	
	

	/**
	* Test For Knight color
	*/	
	@Test
	public void testKnightColor () {
		boolean[][] potentialMove = new boolean[8][8];
		for (int j = 0; j < 8; j++){
			for (int k = 0; k < 8; k++){
				potentialMove[j][k] = false;
			}
		}
		Knight NW = new Knight(3,2,Color.white, potentialMove);
		Knight NB = new Knight(3,2,Color.black, potentialMove);
		
		assertEquals(Color.white, NW.getColor(), "Correct white color");
		assertEquals(Color.black, NB.getColor(), "Correct black color");
		
	

	}
	
	
	
	/**
	* Test For knight potential move
	*/	
	@Test
	public void testKnightPotentialMove () {
		ChessBoard board= new ChessBoard();
		
		boolean[][] potentialMove = new boolean[8][8];
		for (int j = 0; j < 8; j++){
			for (int k = 0; k < 8; k++){
				potentialMove[j][k] = false;
			}
		}
		Knight NW1 = new Knight(4,4,Color.white, potentialMove);
		Knight NW2 = new Knight(3,2,Color.white, potentialMove);
		Knight NB = new Knight(5,2,Color.black, potentialMove);
		board.setFigure(4, 4, NW1);
		board.setFigure(3, 2, NW2);
		board.setFigure(5, 2, NB);
		NW1.setPotentialMove(4, 4, board);
		
		
		
		
		//the normal move
		assertTrue(NW1.getPotentialMove()[6][3], "correct potential move 1");
		
		assertTrue(NW1.getPotentialMove()[6][5], "correct potential move 2");
		
		
		
		//Knight can not move in any other way
		assertFalse(NW1.getPotentialMove()[0][0], "incorrect potential move 1");
		
		//can not capture the own side figures
		assertFalse(NW1.getPotentialMove()[3][2], "incorrect potential move 2");
		
		//capture
		assertTrue(NW1.getPotentialMove()[5][2], "capture");
	

	}
	

	/**
	* Test For knight Unicode
	*/	
	@Test
	public void testKnightUniCode () {
		boolean[][] potentialMove = new boolean[8][8];
		for (int j = 0; j < 8; j++){
			for (int k = 0; k < 8; k++){
				potentialMove[j][k] = false;
			}
		}
		Knight RW = new Knight(3,2,Color.white, potentialMove);
		Knight RB = new Knight(1,2,Color.black, potentialMove);
		
		
		assertEquals("♘", RW.getUnicode(), "white knight");
		assertEquals("♞", RB.getUnicode(), "black knight");
		
		
		
		
	}
}
