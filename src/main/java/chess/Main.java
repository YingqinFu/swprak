
package chess;



import chess.controller.StartGame;
import chess.view.Gui;



/**
 * The common starting point of the GUI and the CLI. 
 * Depending on the given command line arguments either the GUI or the
 * CLI interface are initialized.
 * @author Yingqin Fu, Steffen Häusler, Christoph Cohrs-Thiede from Gruppe 20
 */
public class Main {
    /**
     * The external entry point of the application.
     * @param args The command line arguments passed to the application.
     */
    public static void main(String[] args) {

     boolean cli =  args.length == 1 && args[0].equals("--no-gui");
     
     boolean simple = args.length == 2 && args[0].equals("--no-gui") && 
    		 args[1].equals("--simple") ;
     	
     
        	if (cli) {
        		System.out.println("Welcome to chess game, the match starts now."); 
        		StartGame.InitGame();
        	}
        	if(simple) {
        		StartGame.PlayerVsPlayer();
        	
        	}
        	else {
        		Gui.main(args);
        	}


    }
}
