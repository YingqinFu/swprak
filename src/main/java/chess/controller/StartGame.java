package chess.controller;

import chess.model.Bishop;
import chess.model.ChessBoard;
import chess.model.Color;
import chess.model.King;
import chess.model.Knight;
import chess.model.Pawn;
import chess.model.Queen;
import chess.model.Rook;
import chess.view.Cli;




/**
	 * the class,definite the initial start of the console modus
	 * @author Yingqin Fu, Steffen Häusler, Christoph Cohrs-Thiede from Gruppe 20
	 *
	 */
public class StartGame {
	
	
	/**
	 * to initialize the game
	 */
	public static void InitGame() {
		
		System.out.println("Choose Mode:type 1 for vs Player,type 2 for vs Ai. ");
		
		Input mode =new Input();
		mode.toInsert();
		
		while(!mode.getInput().equals("1")&&!mode.getInput().equals("2")) {
			System.out.println("Error...you must type 1 or 2 to choose a mode.");
			mode.toInsert();
			
		}
		
		if(mode.getInput().equals("1")) {
			PlayerVsPlayer();
		}
		else {
			PlayerVsAi();
			
		}
		
	}
	/**
	 * the "play with other people" modus  of console modus 
	 */
	public static void PlayerVsPlayer() {
			/*System.out.println("Choose color:type 1 for white,type 2 for black. ");
			Input player1 = new Input();
			player1.toInsert();
			while (!player1.getInput().equals("1") && !player1.getInput().equals("2")) {
				System.out.println("Error...you must type 1 or 2 to choose a color.");
				player1.toInsert();
			}
	
			if (player1.getInput().equals("1")) {
				System.out.println("player1 is in white side");
				player1.setActivePlayer(1);
			} else {
				System.out.println("player1 is in black side");
				player1.setActivePlayer(2);
			}*/
	
			ChessBoard board = new ChessBoard();
	
			//initialize the chess board
			initLineup(board);
	
			//call the function run
			RunGame.run(board, "pvp",-1,null,false);
	}


	/**
	 * the modus playerVsAi
	 */
		public static void PlayerVsAi(){
			int level;
			System.out.println("Choose AI:type 1 if you want to play against the simple AI, type 2 if you want to play against the advanced AI. ");
			Input input = new Input();
			input.toInsert();
			while (!input.getInput().equals("1") && !input.getInput().equals("2")) {
				System.out.println("Error...you must type 1 or 2 to choose your opponent.");
				input.toInsert();
			}
			String mode;
			if(input.getInput().equals("1")){
				level = 0;
			}
			else{
				System.out.println("Choose the AI's level from 1 to 3. ");
				String[] levels = {"1","2","3"};
				boolean fits = false;
				input.toInsert();
				for(int i = 0; i<3; i++){
					if(levels[i].equals(input.getInput())){
						fits = true;
					}
				}
				while(!fits){
					System.out.println("Error...you must type a number between 1 and 3 to choose level. ");
					input.toInsert();
					for(int i = 0; i<3; i++){
						if(levels[i].equals(input.getInput())){
							fits = true;
						}
					}
				}
				level = Integer.valueOf(input.getInput());
			}
			System.out.println("Choose color:type 1 for white,type 2 for black. ");
			input.toInsert();
			while (!input.getInput().equals("1") && !input.getInput().equals("2")) {
				System.out.println("Error...you must type 1 or 2 to choose a color.");
				input.toInsert();
			}

			if(input.getInput().equals("1")) {
				System.out.println("player is in white side");
				mode = "pveb";
			}
			else {
				System.out.println("player is in black side");
				mode = "pvew";
			}

			ChessBoard board = new ChessBoard();

			//initialize the chess board
			initLineup(board);

			//call the function run
			RunGame.run(board,mode,level,null,false);
		}

	/**
	 * initialize the chess board
	 * @param board the current board
	 */

	public static void initLineup(ChessBoard board){
		boolean[][] potentialMove = new boolean[8][8];
		
		//initialize figures
		initFigures(board,potentialMove);
		
		//initialize the chess board
		board.initializeBoard();
		
		//print the board 
		Cli.showBoard(board);
		
		
		}
	
	/**
	 * to initialize the figures
	 * @param board the current board
	 * @param potentialMove the potential move 
	 */
	public static void initFigures(ChessBoard board,boolean[][] potentialMove) {
		for (int j = 0; j < 8; j++){
			for (int k = 0; k < 8; k++){
				potentialMove[j][k] = false;
			}
		}
		//initialize the white figures
		initWhiteFigures(board,potentialMove);
		
		//initialize the black figures
		initBlackFigures(board,potentialMove);
		
	}

	/**
	 * to initialize the white figures
	 * @param board the current board
	 * @param potentialMove the potential move 
	 */
	public static void initWhiteFigures(ChessBoard board,boolean[][] potentialMove) {
		Rook rook1w = new Rook(0,7,Color.white, potentialMove);
		Rook rook2w = new Rook(7,7,Color.white, potentialMove);
		Knight knight1w = new Knight(1,7,Color.white, potentialMove);
		Knight knight2w = new Knight(6,7,Color.white, potentialMove);
		Bishop bishop1w = new Bishop(2,7,Color.white, potentialMove);
		Bishop bishop2w = new Bishop(5,7,Color.white, potentialMove);
		Queen queen1w = new Queen(3,7,Color.white, potentialMove);
		King king1w = new King(4,7,Color.white, potentialMove);
		board.setFigure(0,7, rook1w);
		board.setFigure(1,7, knight1w);
		board.setFigure(2,7, bishop1w);
		board.setFigure(3,7, queen1w);
		board.setFigure(4,7, king1w);
		board.setFigure(5,7, bishop2w);
		board.setFigure(6,7, knight2w);
		board.setFigure(7,7, rook2w);
		for (int i = 0;i<8; i++) {
			
			Pawn pawnw = new Pawn(i,6,Color.white, potentialMove);
			
			board.setFigure(i,6, pawnw);
		}
		
	}
	
	/**
	 * to initialize the black figures
	 * @param board the current board
	 * @param potentialMove the potential move 
	 */
	public static void initBlackFigures(ChessBoard board,boolean[][] potentialMove) {
		Rook rook1b = new Rook(0,0,Color.black, potentialMove);
		Rook rook2b = new Rook(7,0,Color.black, potentialMove);
		Knight knight1b = new Knight(1,0,Color.black, potentialMove);
		Knight knight2b = new Knight(6,0,Color.black, potentialMove);
		Bishop bishop1b = new Bishop(2,0,Color.black, potentialMove);
		Bishop bishop2b = new Bishop(0,5,Color.black, potentialMove);
		Queen queen1b = new Queen(3,0,Color.black, potentialMove);
		King king1b	= new King(4,0,Color.black, potentialMove);
		
		board.setFigure(0,0, rook1b);
		board.setFigure(1,0, knight1b);
		board.setFigure(2,0, bishop1b);
		board.setFigure(3,0, queen1b);
		board.setFigure(4,0, king1b);
		board.setFigure(5,0, bishop2b);
		board.setFigure(6,0, knight2b);
		board.setFigure(7,0, rook2b);
		
		for (int i = 0;i<8; i++) {
			Pawn pawnb = new Pawn(i,1,Color.black, potentialMove);
			
			board.setFigure(i,1, pawnb);
			
		}
	}
	
	



}
