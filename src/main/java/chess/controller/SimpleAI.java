package chess.controller;

import chess.model.*;
import java.util.concurrent.ThreadLocalRandom;
/**
 * the class for simple AI
 * @author Yingqin Fu, Steffen Häusler, Christoph Cohrs-Thiede from Gruppe 20
 *
 */
public class SimpleAI implements AI{
	
    /**
     * color (side) which the AI plays (on)
     */
    private Color color;
   
    /**
     * constructor for the AI class
     * @param color (side) which the AI plays (on)
     */
    public SimpleAI(Color color){
        this.color = color;
    }

    /**
     * default constructor of the class
     */
    public SimpleAI(){
    }
    /**
     * getter for color
     * @return color the color of figures
     */
    public Color getColor() {
        return color;
    }
    /**
     * setter for color
     * @param color the color of figures
     */
    public void setColor(Color color) {
        this.color = color;
    }
   
    
    /**
     * to copy the figures
     * @param board the current board
     * @param copyFigures the 2d-array to store the figures
     */
   public void copyFigures(ChessBoard board,Figure[][] copyFigures) {
	   for(int i = 0; i < 8; i++){
           for(int j = 0; j < 8; j++){
               copyFigures[i][j] = board.getFigure(i,j);
           }
       }
	   
   }
   
    /**
     * getter for the best Move
     * @param board current board which the AI analyses
     * @return gives the best move in input form
     */
    @Override
    public String getBestMove(ChessBoard board){
    	Figure[][] copyFigures = new Figure[8][8];
        for(int i = 0; i < 8; i++){
            for(int j = 0; j < 8; j++){
                copyFigures[i][j] = board.getFigure(i,j);
            }
        }
        String bestMoveDef = "";
        String bestMoveAtk = "";
        String bestMove = "";
        int pointsDef = 0;
        int pointsAtk = 0;
        int valBestMove = 0;
        String currentBestMove;

        // going over each figure and finding the best move

        for (int x = 0; x < 8; x++){
            for(int y = 0; y < 8; y++){
                if(board.getFigure(x,y) != null) {

                    // finding the best offensive move due to points gained by a figure

                    if (board.getFigure(x, y).getColor()==this.color) {
                        currentBestMove = bestFigureMove(board,x,y);
                        if(currentBestMove != "") {
                            int cor1 = Input.changeInputToCoordinate(currentBestMove.charAt(3));
                            int cor2 = Input.changeInputToCoordinate(currentBestMove.charAt(4));
                            if (pointsAtk < board.getFigure(cor1, cor2).getPoints()) {
                                pointsAtk = board.getFigure(cor1, cor2).getPoints();
                                bestMoveAtk = currentBestMove;
                            }
                        }
                    }

                    // finding the best defensive move due to points lost by a figure

                    else{
                        currentBestMove = bestFigureMove(board,x,y);
                        if(currentBestMove != "") {
                            int cor1 = Input.changeInputToCoordinate(currentBestMove.charAt(3));
                            int cor2 = Input.changeInputToCoordinate(currentBestMove.charAt(4));
                            if (pointsDef < board.getFigure(cor1, cor2).getPoints()) {
                                pointsDef = board.getFigure(cor1, cor2).getPoints();
                                bestMoveDef = currentBestMove;
                            }
                        }
                    }
                }
            }
        }

        //getting the better move out of the figure values

        if(pointsDef > pointsAtk){
            bestMove = randomMove(board,Input.changeInputToCoordinate(bestMoveDef.charAt(3)),Input.changeInputToCoordinate(bestMoveDef.charAt(4)));
            valBestMove = pointsDef;
            if (bestMove.equals("")){
                bestMove = randomMove(board);
            }
        }
        else{	
            bestMove = bestMoveAtk;
            valBestMove = pointsAtk;
        }
        if(valBestMove == 0){
            bestMove = randomMove(board);
        }
        setAIFigures(board, copyFigures);
        return bestMove;
     }


     /**
      * gives the best move of a figure depending on points it can gain
      * @param board current board which the AI analyses
      * @param x position of the figure
      * @param y position of the figure
      * @return return the best move in form of an input string
      */
     public String bestFigureMove(ChessBoard board, int x, int y){
         String bestMove = "";
         Figure currentFigure = board.getFigure(x,y);
         int maxGain = 0;
         Input input = new Input();
         for(int i = 0; i < 8; i++){
             for(int j = 0; j < 8; j++){
                 input.setInput(transformToInput(x,y,i,j));
                 if(Rules.noRuleBroken(board,input)){
                     if(board.getFigure(i,j) != null && board.getFigure(i,j).getColor() != currentFigure.getColor() ){
                         if(board.getFigure(i,j).getPoints() > maxGain){
                             maxGain = board.getFigure(i,j).getPoints();
                             bestMove = transformToInput(x,y,i,j);
                         }
                     }
                 }
             }
         }
         return bestMove;
    }

   
   /**
    * set the figures of AI
    * @param board the current board
    * @param copyFigures the 2d-array to copy figures
    */
    public static void setAIFigures(ChessBoard board, Figure[][] copyFigures) {
        for(int i = 0; i < 8; i++){
            for(int j = 0; j < 8; j++){
                if(copyFigures[i][j] == null){
                    board.setFigureNull(i,j);
                }
                else{
                    board.setFigure(i,j,copyFigures[i][j]);
                }
            }
        }
        board.initializeBoard();
    	
    }



    /**
     * makes a random move for a random figure
     * @param board current board which the AI analyses
     * @return returns the random move in input string form
     */
    public String randomMove(ChessBoard board) {
        String move = "";
        boolean moveable = false;
      
        for(int x = 0; x < 8; x++) {
            for (int y = 0; y < 8; y++) {
                if (board.getFigure(x, y) != null && board.getFigure(x, y).getColor()==this.color&&moveable(board,x,y)) {
                    
                        moveable = true;
                        break;
                    
                }
            }
        }
        if(moveable){
            move = getRandomMove(board);
        }
        else{
            if(Rules.scanBoardForCheck(board)[0]){
               // System.out.println("checkm8!");
                board.setCheckmate(true);

            }
            else{
               // System.out.println("draw!");
                board.setDraw(true);
            }
            move = "a1-a1";
        }
        return move;
    }

    /**
     * gives a random move for a given figure
     * @param board current board which the AI analyses
     * @param x x-axis position of the figure
     * @param y y-axis position of the figure
     * @return returns the random move in input string form
     */
    public String randomMove(ChessBoard board, int x, int y) {
        String move = "";
        Input input = new Input();
        if(moveable(board,x,y)){
            int i = ThreadLocalRandom.current().nextInt(0,8);
            int j = ThreadLocalRandom.current().nextInt(0,8);
            boolean foundMove = false;
            while(!foundMove){
                i = ThreadLocalRandom.current().nextInt(0,8);
                j = ThreadLocalRandom.current().nextInt(0,8);
                input.setInput(transformToInput(x,y,i,j));
                if(Rules.noRuleBroken(board,input)){
                        foundMove = true;
                }
            }
            move = transformToInput(x,y,i,j);
        }
        return move;
    }


    /**
     * transforms coordinates to an input String
     * @param xStart x-axis position of the figure
     * @param yStart y-axis position of the figure
     * @param xZiel  x-axis target position where the figure wants to move at
     * @param yZiel  y-axis target position where the figure wants to mave at
     * @return the input string
     */
    public static String transformToInput(int xStart, int yStart, int xZiel, int yZiel){
        String input;
        String[] pos = {"a","b","c","d","e","f","g","h"};
        input = pos[xStart]+Integer.toString(8-yStart)+"-"+pos[xZiel]+Integer.toString(8-yZiel);
        return input;
    }

    /**
     * checks if a figure has potential moves
     * @param board is the current board
     * @param x the x-acis position of the figure
     * @param y the y-axis position of the figure
     * @return if the figure is moveable
     */
    public static boolean moveable(ChessBoard board,int x, int y){
        Input input = new Input();
        boolean moveable = false;
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                input.setInput(transformToInput(x, y, i, j));
                if (Rules.noRuleBroken(board, input)) {
                    moveable = true;
                    break;
                }
            }
        }
        return moveable;
    }


    /**
     * is a subfuntion of randomMove an creates the input String if the AI is not check mate
     * @param board the current board
     * @return is the Move in input String form
     */
    public String getRandomMove(ChessBoard board){
        String move = "";
        int x = ThreadLocalRandom.current().nextInt(0,8);
        int y = ThreadLocalRandom.current().nextInt(0,8);
        boolean foundMove = false;
        Input input = new Input();
        if (board.getFigure(x, y) != null && board.getFigure(x, y).getColor()==this.color) {
            if (moveable(board, x, y)) {
                int i = ThreadLocalRandom.current().nextInt(0, 8);
                int j = ThreadLocalRandom.current().nextInt(0, 8);
                
                while (!foundMove) {
                    i = ThreadLocalRandom.current().nextInt(0, 8);
                    j = ThreadLocalRandom.current().nextInt(0, 8);
                    input.setInput(transformToInput(x, y, i, j));
                    if (Rules.noRuleBroken(board, input)) {
                        foundMove = true;
                    }
                }
                move = input.getInput();

            } 
            else {
               move = getRandomMove(board);
            }
        }
        
        else{
            move = getRandomMove(board);
        }
        return move;
    }
    
   


}
