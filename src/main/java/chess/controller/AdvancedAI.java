package chess.controller;

import chess.model.ChessBoard;
import chess.model.Color;
import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;

/**
 * is the advanced AI working with minimax algorithm and alpha-beta-pruning to find the best possible move
 * @author Yingqin Fu, Steffen Häusler, Christoph Cohrs-Thiede from Gruppe 20
 */
public class AdvancedAI implements AI{

    /**
     * color (side) which the AI plays (on)
     */
    private Color color;

    /**
     * the level of the AI
     */
    private int level;

    /**
     * the "helper" for the advanced AI
     * to implement methods of SimpleAI
     */
    private SimpleAI helper;


    /**
     * Constructor of the AI
     * @param color the color the AI plays as
     * @param level the level of the AI
     */
    public AdvancedAI(Color color, int level){
        this.level = level;
        this.color = color;
        this.helper = new SimpleAI(color);
    }

    /**
     * get-method of level
     * @return the level 
     */
    public int getLevel() {
        return level;
    }
    /**
     * set-method of level
     * @param level the level
     */
    public void setLevel(int level) {
        this.level = level;
    }
    /**
     * get method of color
     * @return the color of ai
     */
    public Color getColor() {
        return color;
    }
    /**
     * set-method of color
     * @param color the color of ai
     */
    public void setColor(Color color) {
        this.color = color;
    }
    /**
     * get-methode of helper
     * @return helper  the "helper" for the advanced AI
     */
    public SimpleAI getHelper() {
        return helper;
    }

    /**
     * gives you the best move for the AI
     * @param board the current board
     * @return The move in String form
     */
    @Override
    public String getBestMove(ChessBoard board){
        ChessBoard copy = RunGame.boardcopy(board);
        return minimaxChess(copy,this.level,0,"",Integer.MIN_VALUE,Integer.MAX_VALUE)[0];
    }

    /**
     * the minimax algorithm for the AI valueing moves and returning the best one
     * @param board the current board
     * @param depth the search depth
     * @param points the value for moves that been made
     * @param start the first move done by the AI
     * @param alpha the alpha value for alpha-beta-pruning
     * @param beta the beta value for alpha-beta-pruning
     * @return returns the most valueable move for the AI
     */
    public String[] minimaxChess(ChessBoard board,int depth,int points,String start, int alpha, int beta){
        ArrayList<String> moveList = new ArrayList<>();
        String[] ret = new String[2];
        if(depth == 0){
        ret[0] = start;
        ret[1] = String.valueOf(points);
        return ret;
        }

        //Branch of the maximising Player

        if((this.level-depth) % 2 == 0){
            int maxEval = Integer.MIN_VALUE;
            breakpointMax:
            for(int i = 0; i < 8; i++){
                for(int j = 0; j < 8; j++){
                    if(board.getFigure(i, j) != null && board.getFigure(i, j).getColor()==this.color){
                        for(int x = 0; x < 8; x++){
                            for(int y = 0;y < 8; y++){
                                Input input = new Input();
                                ChessBoard copy = RunGame.boardcopy(board);
                                String inputString = SimpleAI.transformToInput(i,j,x,y);
                                input.setInput(inputString);
                                String startMove;
                                int figurePoints = 0;
                                if(Rules.noRuleBroken(copy,input)){
                                    if(depth == level){
                                        startMove = input.getInput();
                                    }
                                    else{
                                        startMove = start;
                                    }
                                    if(board.getFigure(x,y) != null && board.getFigure(x,y).getColor()!=this.color){
                                        figurePoints = copy.getFigure(x,y).getPoints();
                                    }
                                    RunGame.makeChessMove(copy,input);
                                    String [] moveAndPoints = minimaxChess(copy,depth-1,points+figurePoints,startMove,alpha,beta);
                                    if(!moveAndPoints[1].equals(null)){

                                        // getting the best Move for AI

                                    if(maxEval < Integer.valueOf(moveAndPoints[1])){
                                        moveList.clear();
                                        moveList.add(moveAndPoints[0]);
                                        alpha = Integer.valueOf(moveAndPoints[1]);
                                        maxEval = Integer.valueOf(moveAndPoints[1]);
                                        ret[1] = moveAndPoints[1];
                                    }
                                    if(maxEval == Integer.valueOf(moveAndPoints[1])){
                                        moveList.add(moveAndPoints[0]);
                                    }

                                    // pruning of knots

                                    if(beta < alpha) {
                                        break breakpointMax;
                                    }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if(moveList.size() == 0){
                moveList.add(helper.getBestMove(board));
                ret[1] = String.valueOf(points);
            }
            int x = ThreadLocalRandom.current().nextInt(0, moveList.size());
            ret[0] = moveList.get(x);
            return ret;
        }

        //Branch of the minimising Player

        else{
            int minEval = Integer.MAX_VALUE;
            breakpointMin:
            for(int i = 0; i < 8; i++){
                for(int j = 0; j < 8; j++){
                    if(board.getFigure(i, j) != null && board.getFigure(i, j).getColor()!=this.color){
                        for(int x = 0; x < 8; x++){
                            for(int y = 0; y < 8; y++){
                                Input input = new Input();
                                ChessBoard copy = RunGame.boardcopy(board);
                                String inputString = SimpleAI.transformToInput(i,j,x,y);
                                input.setInput(inputString);
                                int figurePoints = 0;
                                if(Rules.noRuleBroken(copy,input)){
                                    if(board.getFigure(x,y) != null && board.getFigure(x,y).getColor()==this.color){
                                        figurePoints = copy.getFigure(x,y).getPoints();
                                    }
                                    RunGame.makeChessMove(copy,input);
                                    String [] moveAndPoints = minimaxChess(copy,depth-1,points-figurePoints,start,alpha,beta);
                                    if(moveAndPoints[1] != null) {
                                        // getting the best Move for opponent
                                        if (minEval > Integer.valueOf(moveAndPoints[1])) {
                                            moveList.clear();
                                            moveList.add(moveAndPoints[0]);
                                            beta = Integer.valueOf(moveAndPoints[1]);
                                            minEval = Integer.valueOf(moveAndPoints[1]);
                                            ret[1] = moveAndPoints[1];
                                        }
                                        if (minEval == Integer.valueOf(moveAndPoints[1])) {
                                            moveList.add(moveAndPoints[0]);
                                        }
                                        // pruning of knots

                                        if (beta < alpha) {
                                            break breakpointMin;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if(moveList.size() == 0){
                moveList.add(helper.getBestMove(board));
                ret[1] = String.valueOf(points);
            }
            int x = ThreadLocalRandom.current().nextInt(0, moveList.size());
            ret[0] = moveList.get(x);
            return ret;
        }
    }


}