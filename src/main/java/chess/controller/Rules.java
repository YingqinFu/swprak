package chess.controller;

import chess.model.ChessBoard;
import chess.model.Color;
import chess.model.Figure;

import java.util.List;


/**
	 * the class,definite the fundamental rules of all figures
	 * @author Yingqin Fu, Steffen Häusler, Christoph Cohrs-Thiede from Gruppe 20
	 *
	 */
public class Rules {
	
	/**
	 * the position of king
	 */
	private static int[] kingPos = new int[4];
	
	
	
	/**
	 * to check,if the rules are broken or not
	 * @param board the currant board
	 * @param input the input of users
	 * @return noRulesBroken is there no rules broken
	 */
	
    public static boolean noRuleBroken(ChessBoard board, Input input){
    	
    	boolean noRulesBroken=false;
		int sourceX = Input.changeInputToCoordinate(input.getInput().charAt(0));
		int sourceY = Input.changeInputToCoordinate(input.getInput().charAt(1));
		int targetX = Input.changeInputToCoordinate(input.getInput().charAt(3));
		int targetY = Input.changeInputToCoordinate(input.getInput().charAt(4));

		//to check,whether the figures exist at current source position or not.

		if (board.getFigure(sourceX, sourceY) != null) {
			//get the figures at current source position.
			board.getFigure(sourceX, sourceY).setPotentialMove(sourceX, sourceY, board);

			//check the en passant rules for the white pawn
			board.getFigure(sourceX, sourceY).whiteEnPassant(input, board);

			//check the en passant rules for the black pawn
			board.getFigure(sourceX, sourceY).blackEnPassant(input, board);

			//check the castling move for the white king
			board.getFigure(sourceX, sourceY).whiteCastling(input, board);

			//check the castling move for the black king
			board.getFigure(sourceX, sourceY).blackCastling(input, board);


			//to check,if the target position is a potential move or not
			if (board.getFigure(sourceX, sourceY).getPotentialMove()[targetX][targetY]) {

				boolean[] test = checkCheck(board, input);
				
				if (test[0] && test[1] && board.getFigure(sourceX, sourceY).getColor()==Color.white) {
					
					noRulesBroken=false;
					
				} 
				
				else if (test[0] && !test[1] && board.getFigure(sourceX, sourceY).getColor()==Color.black) {
					
					noRulesBroken=false;
					
				} 
				else {
					
					noRulesBroken=true;
				}
			}

		}
		return noRulesBroken;
		

		}


		/**
		 * 
		 * gives information if a king is in check after the input move and which king it is
		 * @param board the current board
		 * @param input the move that the player wants to play
		 * @return returns the current information about kings in check in an boolean array.
		 * the returns decoded means (false,false) no king in check, (true, false) black king in check
		 *  (true, true) white king in check. (false, true) is not a possible return.
		 */
    public static boolean[] checkCheck(ChessBoard board, Input input){

		int sourceX=Input.changeInputToCoordinate(input.getInput().charAt(0));
		int sourceY=Input.changeInputToCoordinate(input.getInput().charAt(1));
		int targetX=Input.changeInputToCoordinate(input.getInput().charAt(3));
		int targetY=Input.changeInputToCoordinate(input.getInput().charAt(4));

        Figure[][] copyFigures = new Figure[8][8];
        for(int i = 0; i < 8; i++){
        	for(int j = 0; j < 8; j++){
        		copyFigures[i][j] = board.getFigure(i,j);
			}
		}
        board.setFigure(targetX,targetY,board.getFigure(sourceX,sourceY));
        board.setFigureNull(sourceX,sourceY);
        boolean[] check = scanBoardForCheck(board);
        for(int i = 0; i<8; i++){
        	for(int j = 0; j < 8; j++){
        		if(copyFigures[i][j] == null){
        			board.setFigureNull(i,j);
				}
        		else{
        			board.setFigure(i,j,copyFigures[i][j]);
				}
			}
		}
        return check;
	}
		

		/**
		 * search for king 
		 * 
		 * @param board the Chess board the game runs on
		 * @return true if any King is indirect Danger
		 */

		public static boolean[] scanBoardForCheck(ChessBoard board){
	    	boolean[] output = {false,false};
	    	
	    	//getting the King positions
	    	for (int i = 0; i<8;i++){
	    		for (int j = 0; j<8;j++){
	    			if (board.getFigure(i,j) != null){
						if (board.getFigure(i,j).getName() == "K"){
							kingPos[0] = i;
							kingPos[1] = j;
						}
						else if (board.getFigure(i,j).getName() == "k"){
							kingPos[2] = i;
							kingPos[3] = j;
						}
					}

				}
			}
	    	KingInCheck(board,output);
	    	
			

	    	return output;
		}

		/**
		 * this one searches the Field to decide if any King is in direct Danger
		 * @param board the current board
		 * @param output the output, whether a king is in danger or not
		 */
		public static void KingInCheck(ChessBoard board,boolean[] output) {
			for (int i = 0; i<8;i++) {
				for (int j = 0; j < 8; j++) {
					if (board.getFigure(i,j) != null && board.getFigure(i,j).getName() != "K"&& board.getFigure(i,j).getName() != "k"){
						
						board.getFigure(i,j).setPotentialMove(i,j,board);
						
						
						Checking(board,i,j,output);
						

						
					}

				}
			}
			
		}
		 /**
		  * checking for black and white king
		  * @param board the current board
		  * @param i the x-axis
		  * @param j the y-axis
		  * @param output the output
		  */
		public static void Checking(ChessBoard board, int i, int j, boolean[] output) {
			boolean [][] array = board.getFigure(i,j).getPotentialMove();
			
			//Black King in Check
			 if(board.getFigure(i,j).getColor()==Color.white && array[kingPos[2]][kingPos[3]]){
					output[0] = true;
				}
				//White King in Check
				if(board.getFigure(i,j).getColor()==Color.black&& array[kingPos[0]][kingPos[1]]) {
					output[0] = true;
				    output[1] = true;
				}
			
		}
		/**
		 * the finish of game
		 * @param board the current board
		 * @param activeColor the active color
		 * @return finished or not
		 */
		public static boolean gameFinished(ChessBoard board, Color activeColor) {
			//String move = "";
			boolean moveable = false;
			breakpoint:
			for (int x = 0; x < 8; x++) {
				for (int y = 0; y < 8; y++) {
					if (board.getFigure(x, y) != null && board.getFigure(x, y).getColor()==activeColor && SimpleAI.moveable(board, x, y)) {
						moveable = true;
						break breakpoint;
					}
				}
			}
			if (!moveable) {
				if (Rules.scanBoardForCheck(board)[0]) {
					board.setCheckmate(true);
				} else {
					board.setDraw(true);
				}
			}
			if(remi(board)) {
				moveable = false;
				board.setDraw(true);
			}
			return !moveable;

		}
		/**
		 * remi rules for game
		 * @param board the current board
		 * @return remi or not
		 */
		public static boolean remi(ChessBoard board){
			boolean remi = false;
			List<String> moves = board.getMovedList();
			int length = moves.size();
			if(length >= 12) {
				remi = true;
				for (int i = 0; i < 4; i++) {
					if (!moves.get(length-1-i).equals(moves.get(length-5-i))) {
						remi = false;
					}
				}
			}
         return remi;
		}
    
	  
}