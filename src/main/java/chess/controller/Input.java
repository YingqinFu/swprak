package chess.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import chess.model.Color;
	/**
	 * the class,definite the details about the Input of users
	 * @author Yingqin Fu, Steffen Häusler, Christoph Cohrs-Thiede from Gruppe 20
	 *
	 */
public class Input {
	/**
	 * the String of the Input
	 */
	private String insert;
	
	/**
	 * the current player
	 */
	private int activePlayer = 1;
	
	
	/**
	 * users can input the moves
	 */
	
	public void toInsert() {
		
		@SuppressWarnings("resource")
		final Scanner scanner = new Scanner(System.in);
		this.insert = scanner.nextLine();
	}
	
	
	
	/**
	 * setter-method for input
	 * will be used in Test class
	 * @param input the input of users
	 */
	public void setInput(String input) {
		this.insert=input;
	}
	
	
	/**
	 * switch the current player
	 */
	public void switchPlayers(){
		//the current side is white
			if(activePlayer == 1){
				//System.out.println("black side,please enter your next move: ");
				activePlayer = 2;
			}
		//the current side is black
			else{
				//System.out.println("white side,please enter your next move: "); 
				activePlayer = 1;
			}
	}
	
	
	/**
	 * get-method of activePlayer
	 * @return activePlayer
	 */
	public int getActivePlayer() {
		return activePlayer;
	}
	/**
	 * get-method of active player
	 * @return the color of players
	 */
	public Color getActivePlayerColor(){
		
		if(this.getActivePlayer() == 1){
			return Color.white;
		}
		else {
			return Color.black;
		}
	}
	
	
	/**
	 * set-method of activePlayer
	 * @param activePlayer the current player
	 */
	public void setActivePlayer(int activePlayer) {
		this.activePlayer=activePlayer;
	}
	

	
	
	/**
	 * get-method of input
	 * @return input the input of users
	 */
	public String getInput() {
		return insert;
	}
	
	
	/**
	 * switch the Input of users to the Coordinate of Chess board
	 * @param makeMove the char of input
	 * @return coordinate the coordinate of board
	 */
	public static int changeInputToCoordinate(char makeMove){
		int coordinate=-2;
		
		switch (makeMove) {
		
		case 'a':
		case '8':
			coordinate =0;
			break;
			
		case 'b':
		case '7':
			coordinate=1;
			break;
			
		case 'c':
		case '6':
			coordinate=2;
			break;
			
		case 'd':
		case '5':
			coordinate=3;
			break;
			
		
	
		default:
			break;
			
			
		}
		return changeInputToCoordinate1(makeMove,coordinate);
		
	}
	
	
	/**
	 * switch the Input of users to the Coordinate of Chess board 
	 * @param makeMove the char of input
	 * @param coordinate the coordinate of board
	 * @return coordinate the coordinate of board
	 */
	public static int changeInputToCoordinate1(char makeMove,int coordinate){
		
		int coordinate1=coordinate;
		switch (makeMove) {
		
		case 'e':
		case '4':
			coordinate1=4;
			break;
			
		case 'f':
		case '3':
			coordinate1=5;
			break;
			
		case 'g':
		case '2':
			coordinate1=6;
			break;
			
		case 'h':
		case '1':
			coordinate1=7;
			break;
	
		default:
			
			break;
			
			
		}
		return coordinate1;
		
	}
	/**
	 * to check,if the input of users invalid or not
	 * @param cnslinput the input of users
	 * @return invalidInput invalidInput or not
	 */
	public static boolean invalidInput(Input cnslinput) {
		boolean invalidInput = true;
		final List<Character> listLetters = Arrays.asList('a','b','c','d','e','f','g','h');
		final List<Character> listNumbers = Arrays.asList('1','2','3','4','5','6','7','8');
		if(cnslinput.getInput().length() == 5||cnslinput.getInput().length() == 6) {
			final char[] cnslchar = cnslinput.getInput().toCharArray();
			if (listLetters.contains(cnslchar[0])&&listNumbers.contains(cnslchar[1])&&cnslchar[2] == '-'&&listLetters.contains(cnslchar[3])&&listNumbers.contains(cnslchar[4])) {
					invalidInput = false;
						
				}
			}
			
		return invalidInput;
	}
	
	/**
	 * to check,if the input of users is a promotion of pawn
	 * @param cnslinput the input of users
	 * @return invalidInput invalid input or not
	 */
	public static boolean invalidInputWithPromotion(Input cnslinput ) {
	
			
		boolean invalidInput=true;
		
		if (cnslinput.getInput().length() == 5&&!invalidInput(cnslinput)) {
			invalidInput=false;
			//return false;
			
		}
		
		
		if(!invalidInput(cnslinput)&&cnslinput.getInput().length()==6) {
			
			//four possible promotion figures
			switch(cnslinput.getInput().charAt(5)) {
			
			case 'Q':
			case 'R':
			case 'B':
			case 'N':
				invalidInput=false;
				break;
				
			default:
				invalidInput=true;
				break;
			}
			
		 }
		
	
		
		return invalidInput;
		
	}
	
	
	

	
}
