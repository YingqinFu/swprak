package chess.controller;

import chess.model.ChessBoard;
/**
 * the interface of Ai
 * @author Yingqin Fu, Steffen Häusler, Christoph Cohrs-Thiede from Gruppe 20
 *
 */
public interface AI {

	/**
	 * gives the best move in regard to the current board and color of the AI
	 * @param board the current board
	 * @return the best move
	 */
     String getBestMove(ChessBoard board);

}
