package chess.controller;
	
import chess.model.ChessBoard;
import chess.model.Color;
import chess.model.Figure;
import chess.view.Cli;
	/**
	 * the class,keep the game always running
	 * @author Yingqin Fu, Steffen Häusler, Christoph Cohrs-Thiede from Gruppe 20
	 *
	 */
public class RunGame {
	
	
	/**
	 * 2d-Array to store the current figures of board(aims at the bugs between en passant(castling) with king in check)
	 */
	 public static Figure[][] currentBoard =new Figure[8][8];
	 
	 /**
	  * 2d-Array to store the current figures of board(aims at the bugs between en passant(castling) with king in check)
	  */
	 
	 public static Figure[][] BoardWithoutChange =new Figure[8][8];
	 
	
	 /**
	  * to run the whole console-modus
	  * @param board the currant board
	  * @param mode the mode of game
	  * @param playOnGui the GUI modus or not
	  * @param level the level of AI
	  * @param input the input of users
	  */
    public static void run(ChessBoard board,String mode,int level,Input input, boolean playOnGui){
    	if(playOnGui){
    		
    		RunForGUI(board,input);
		
		}
        else {
			//System.out.println("white side, please enter your first move: ");
			Input cnslInput = new Input();

			//get the new input of users
			getNewInput(mode, cnslInput);

			//set the AI modus
			//boolean simpleAI = false;
			AdvancedAI aI;
			if (mode.equals("pveb")) {
				aI = new AdvancedAI(Color.black, level);
			} else {
				aI = new AdvancedAI(Color.white, level);
			}


			// keep running the game, when black and white king are alive
			while (FinishGame.whiteKingAlive(board) && FinishGame.blackKingAlive(board)) {

				//get the input of AI
				setAIinput(mode, cnslInput, board, aI, level);


				//always check, whether the input is valid or not, or the input is beaten
				while (!Input.invalidInputWithPromotion(cnslInput) || cnslInput.getInput().equals("beaten")) {

					//get the input of AI
					setAIinput(mode, cnslInput, board, aI, level);

					//print the beaten list
					printBeatenList(board, cnslInput);

					//get the initial board
					boardBackup(board);

					//always check,whether the move broke the rules, and is the right turn or not
					if (Rules.noRuleBroken(board, cnslInput) && checkRightTurn(board, cnslInput)) {

						//get the board,after the method noRuleBroken
						boardSafe(board);
						
						if(!mode.equals("pvp")){
							System.out.println("waiting for AI...");
						}

						//always check, whether a king will be in check after a move of his own side or not
						moveWithoutCheck(board, cnslInput);


					}

					//the move broke the rules
					else {


					if (!cnslInput.getInput().equals("beaten")) {

						//print "!Move not allowed",when the input of users break the rules
						System.out.println("!Move not allowed");

					}

						//if a king is in check,don't update the board
						returnBackup(board);

					}

						//get the new input of users
						getNewInput(mode, cnslInput);


				}
						//print message
						printInvalidOrEnd(board, cnslInput);

			}
		}
    }
    
    /**
     * to get the new input of users
     * @param mode the mode of game
     * @param cnslInput the input of users
     */
    public static  void getNewInput(String mode, Input cnslInput) {
    	
    	//if the users choose white, get the input of white side
    	if(mode.equals("pveb") && cnslInput.getActivePlayer() == 1) {
			cnslInput.toInsert();
		}
    	
    	//if the users choose black, get the input of black side
    	if(mode.equals("pvew") && cnslInput.getActivePlayer() == 2) {
			cnslInput.toInsert();
		}
    	
    	//the PlayerVsPlayer modus
    	if(mode.equals("pvp") ) {
			cnslInput.toInsert();
		}
    }
    
    /**
	  * set the input of AI
	  * @param mode the mode of game
	  * @param cnslInput the input of users
	  * @param board the current board
	  * @param aI the AI modus
	  * @param level the level of AI
	  */
	public static void setAIinput(String mode,Input cnslInput,ChessBoard board,AdvancedAI aI,int level) {
		
		if(mode.equals("pveb") && cnslInput.getActivePlayer() == 2){
			if(level == 0){
				cnslInput.setInput(aI.getHelper().getBestMove(board));
			}
			else{
				cnslInput.setInput(aI.getBestMove(board));
			}
		}
		else if(mode.equals("pvew") && cnslInput.getActivePlayer() == 1){
			if(level == 0){
				cnslInput.setInput(aI.getHelper().getBestMove(board));
			}
			else{
				cnslInput.setInput(aI.getBestMove(board));
			}
		}
		
	}
  
    /**
     * print the finish game message or "!Invalid move"
     * @param board the currant board
     * @param cnslInput the input of users
     */
    public static void printInvalidOrEnd(ChessBoard board,Input cnslInput) {
    		//go to end the game
			if( !FinishGame.whiteKingAlive(board)||!FinishGame.blackKingAlive(board)) {
				System.out.println("!Game Over");
			
			}
			else {
		    //print "!Invalid move",when the input 	of users have syntactic mistakes
			System.out.println("!Invalid move");
		
		    //get the new input of users
			cnslInput.toInsert();
			}
    	
    }

    /**
     * run the game, when there is no king in check
     * @param board the current board
     * @param cnslInput the input of users
     */
    public static void moveWithoutCheck(ChessBoard board,Input cnslInput) {
		  		
 			  //return to the initial board
 			  returnSafe(board);
 		  	
 			  //confirm the correct input
 			  System.out.println("!"+cnslInput.getInput());
		  
 			  //make the move of figures
 			  makeChessMove(board, cnslInput);
		  
 			  //initialize the new board
 			  board.initializeBoard();
	
 			  //print the board 
 			  Cli.showBoard(board);
	
 			  //switch the current player
 			  cnslInput.switchPlayers();	
 			  
    }
    
    /**
     * the function,to make the Chess move to the target position
     * @param board the currant board
     * @param input the input of users
     */
    public static void makeChessMove(ChessBoard board,Input input) {
    	int sourceX=Input.changeInputToCoordinate(input.getInput().charAt(0));
    	int sourceY=Input.changeInputToCoordinate(input.getInput().charAt(1));	
    	int targetX=Input.changeInputToCoordinate(input.getInput().charAt(3));
    	int targetY=Input.changeInputToCoordinate(input.getInput().charAt(4));
    	
		 //add the beaten figures into the beaten list
		if(board.getFigure(targetX, targetY)!=null) {
	   		 board.getBeatenList().add(board.getFigure(targetX, targetY).getName());
	    	}
    	
    	 //set the figures at the source position to the target position
    	 board.setFigure(targetX,targetY,board.getFigure(sourceX, sourceY));
    	
         //set the figures at the source position,if a promotion of white pawn occurs	
         board.getFigure(sourceX, sourceY).whitePromotion(input, board);
         
         //set the figures at the source position,if a promotion of black pawn occurs	
         board.getFigure(sourceX, sourceY).blackPromotion(input, board);
         
         //add the move into the move list
         board.getMovedList().add(input.getInput());
    	
    	 //set the figures at the source position null
         board.setFigureNull(sourceX, sourceY);
      
    }
    
    /**
     * the function,to check whether the current move is with the right side of figures or not
     * @param board the currant board
     * @param input the input of users
     * @return rightTurn the right turn or  not
     */
    public static boolean checkRightTurn(ChessBoard board,Input input) {
    	
    	boolean rightTurn=false;
    	int sourceX=Input.changeInputToCoordinate(input.getInput().charAt(0));
    	int sourceY=Input.changeInputToCoordinate(input.getInput().charAt(1));	
    	
    	
    	//only the figures of white side can make move
    	if(board.getFigure(sourceX, sourceY).getColor()==Color.white&&input.getActivePlayer()==1) {
    	
    			rightTurn=true;
    		
    		
    	}
    	//only the figures of black side can make move
    	else if(board.getFigure(sourceX, sourceY).getColor()==Color.black&&input.getActivePlayer()==2) {
    		
    			rightTurn=true;
    		
    		
    	}
    	return rightTurn;
    	
 
    	
    }
    /**
     * print the beaten figures,when users enter "beaten" in console
     * @param board the current board
     * @param cnslInput the input of users
     */ 
    public static void printBeatenList(ChessBoard board, Input cnslInput) {
    	
    	if(cnslInput.getInput().equals("beaten")) {
    		for (String i : board.getBeatenList()) {
			            System.out.println(i);
			        }
				  
			  }
		 
	 }
    
    /**
     * get the current board1
     * @param board the current board
     */
    public static void boardBackup(ChessBoard board) {
    	for(int x =0; x<8;x++) {
 			for(int y =0; y<8;y++) {
 				currentBoard[x][y]=board.getFigure(x, y);
 				
 				}
 			}
    	
    	
    }
    
    /**
     * set the update board to old board1
     * @param board the current board
     */
    public static void returnBackup(ChessBoard board) {
    	 for(int x =0; x<8;x++) {
   			for(int y =0; y<8;y++) {
   				if(currentBoard[x][y]==null) {
   					board.setFigureNull(x, y);
   				}
   				else {
   				board.setFigure(x, y, currentBoard[x][y]);
   				}
   				
   				}
   			}
    }
    
    /**
     * get the current board2
     * @param board the current board
     */
    public static void boardSafe(ChessBoard board) {
    	for(int x =0; x<8;x++) {
 			for(int y =0; y<8;y++) {
 				BoardWithoutChange[x][y]=board.getFigure(x, y);
 				
 				}
 			}
    	
    	
    }
    
    /**
     * set the update board to old board2
     * @param board the current board
     */
    public static void returnSafe(ChessBoard board) {
    	 for(int x =0; x<8;x++) {
   			for(int y =0; y<8;y++) {
   				if(BoardWithoutChange[x][y]==null) {
   					board.setFigureNull(x, y);
   				}
   				else {
   				board.setFigure(x, y, BoardWithoutChange[x][y]);
   				}
   				
   				}
   			}
    }
    
  
		/**
		 * copy the board
		 * @param board the current board
		 * @return copy the copy one
		 */ 
		public static ChessBoard boardcopy(ChessBoard board){
			Figure[][] copyFigures = new Figure[8][8];
			for(int i = 0; i < 8; i++){
				for(int j = 0; j < 8; j++){
					copyFigures[i][j] = board.getFigure(i,j);
				}
			}
			ChessBoard copy = new ChessBoard(copyFigures);
			copy.initializeBoard();
			return copy;
		}
		/**
		 * Run for GUI modus
		 * @param board the current board
		 * @param input the input of users
		 */
		public static void RunForGUI(ChessBoard board,Input input) {
			board.setLastMoveValid(false);
			if (FinishGame.whiteKingAlive(board) && FinishGame.blackKingAlive(board)){
				boardBackup(board);
				if (Rules.noRuleBroken(board, input) && checkRightTurn(board, input)){

					boardSafe(board);

					moveWithoutCheck(board, input);
					
					board.setLastMoveValid(true);

				}
				else {

					returnBackup(board);

				}
			}
		}
    
    
}
