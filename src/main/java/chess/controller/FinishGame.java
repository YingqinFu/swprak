package chess.controller;

import chess.model.ChessBoard;
/**
 * the class, to check whether the game is going to finish
 * @author Yingqin Fu, Steffen Häusler, Christoph Cohrs-Thiede from Gruppe 20
 *
 */
public class FinishGame {
	/**
	 * method check,whether a king of white side is still alive
	 * @param board the current board
	 * @return whiteKing is alive or not
	 */
	 public static boolean whiteKingAlive(ChessBoard board){
		 boolean whiteKingIsAlive=false;
		 for(int i =0; i<8;i++) {
	   			for(int j =0; j<8;j++) {
	   				if(board.getFigure(i, j)!=null&&board.getFigure(i, j).getName().equals("K")) {
	   					whiteKingIsAlive=true;
	   					
	   					
	   				}
	   				
	   			}
	   			
		   }
	    	
			return whiteKingIsAlive;
		 
	 }
	 /**
		 * method to check,whether a king of black side is still alive
		 * @param board the currant board
		 * @return black King alive or not
		 */
	 public static boolean blackKingAlive(ChessBoard board){
		 boolean blackKingIsAlive=false;
		 for(int i =0; i<8;i++) {
	   			for(int j =0; j<8;j++) {
	   				
	   				if(board.getFigure(i, j)!=null&&board.getFigure(i, j).getName().equals("k")) {
	   					
	   					blackKingIsAlive= true;
	   					
	   				 }
	   			}
		   }
	    	
			return blackKingIsAlive;
		 
	 }
	 
	 

}
