package chess.view;

import chess.model.ChessBoard;
	/**
	 * the view class of console modus
	 * @author Yingqin Fu, Steffen Häusler, Christoph Cohrs-Thiede from Gruppe 20
	 *
	 */
public class Cli {
	/**
	 * the function,to print the chess board
	 * @param board the current board
	 */
    @SuppressWarnings("exports")
	public static void showBoard(ChessBoard board) {
    	if (!board.isGuiMode() )
			System.out.println(board.getField());
    }
}
