package chess.view;

import chess.controller.*;
import chess.model.ChessBoard;
import chess.model.Color;
import javafx.animation.PauseTransition;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

import java.util.Objects;


/**
 * Starting point of the JavaFX GUI
 * @author Yingqin Fu, Steffen Häusler, Christoph Cohrs-Thiede from Gruppe 20
 */
public class Gui extends Application {
    /**
     * Used to convert the numbers to Letters for the Field
     */
    String toLetters = "ABCDEFGH";
    /**
     * The IP-Address to Join a game,
     * by default it is set to "localhost"
     */
    String ipAddress = "localhost";
    /**
     * Used to give the Labels their names
     */
    String [] labelId = new String[]{"historyField","beatenField","messageField","aiLevel"};
    /**
     * Used to set the difficulty level for the advanced AI
     */
    String [] difficulty = new String[]{"Easy","Medium","Hard"};
    /**
     * Input used to make the Moves in the run() method
     */
    Input input = new Input();
    /**
     * The simple AI to play against
     */
    SimpleAI simpleAi = new SimpleAI();
    /**
     * The advanced AI to play against
     *
     */
    AdvancedAI advancedAi = new AdvancedAI(Color.white, 1);
    /**
     * boolean that saves if the simple AI was selected
     */
    boolean simpleAiSelected = true;
    /**
     * boolean that saves if a local game was selected
     */
    boolean localGameSelected = true;
    /**
     * boolean that saves if hosting was selected
     */
    boolean hostingSelected = true;
    /**
     * boolean used to stop the game if draw or check occurs
     */
    boolean gameRunning = true;
    /**
     * The Socket used to Join a Game
     */
    Socket client;
    /**
     * The Serversocket used to Host a Game
     */
    ServerSocket server;
    /**
     * The Printwriter used to send Inputs over Network
     */
    PrintWriter printWriter;
    /**
     * The Inputstreamreader used to read Inputs from the Network
     */
    InputStreamReader inputStreamReader;
    /**
     * The BufferedReader used in combination with the
     * Inputstreamreader to read Inputs from the Network
     */
    BufferedReader bufferedReader;
    /**
     * This PauseTransition is used to make sure JavaFX will draw the correct Field
     * before waiting for an Opponent
     */
    PauseTransition networkPause = new PauseTransition(Duration.seconds(1));

    /**
     * This PauseTransition is used to display the "Waiting for Connection" Message while hosting
     */
    PauseTransition initFieldPause = new PauseTransition(Duration.millis(100));

    /**
     * This PauseTransition is used to display the "Waiting for Opponent" Message
     * after your move has been made and transmitted
     */
    PauseTransition waitForOpponentPause = new PauseTransition(Duration.millis(100));

    /**
     * This PauseTransition is used to initialize the Server and the Client after the Field has been drawn
     */
    PauseTransition initServerClientPause = new PauseTransition(Duration.seconds(1));

    /**
     * This PauseTransition is used to display that the AI is working on the next move
     */
    PauseTransition aiPause = new PauseTransition(Duration.seconds(1));



    /**
     * This method is called by the Application to start the GUI.
     * Here, all the necessary elements of the 2D Gui Chessboard will be created
     *
     * @param primaryStage The initial root stage of the application.
     */
    @SuppressWarnings({"PMD.NcssCount", "TooManyFields"})
    /*
     * Here the NCSS and TooManyFields PMD warning is suppressed. This method has a lot of instructions, too much. But in this
     * case, there is no other way since the start() method is the only place where the complex design of the GUI
     * can be defined, that leads to a lot of instructions in this case.
     */
    @Override
    public void start(Stage primaryStage) {

        ChessBoard board = new ChessBoard();
        board.setGuiMode(true);

        primaryStage.setTitle("Chess");
        GridPane chooseMode = new GridPane();
        GridPane chooseColor = new GridPane();
        GridPane networkGame = new GridPane();
        GridPane chessGrid = new GridPane();
        networkPause.setOnFinished(event -> {
            try {
                input.setInput(bufferedReader.readLine().toLowerCase());
                System.out.println(input.getInput());
                if (gameRunning){
                    RunGame.run(board,null,-1,input,true);
                    if(!board.isLastMoveValid()){
                        if(input.getInput().equals("c1-c1")){
                            printWriter.println("d1-d1");
                        }
                        else{
                            printWriter.println("b1-b1");
                        }
                        printWriter.flush();
                        endGame(primaryStage);

                    }
                }


                drawField(chessGrid, board,"");
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        initFieldPause.setOnFinished(event -> drawField(chessGrid,board,"Waiting for Connection.."));

        waitForOpponentPause.setOnFinished(event -> drawField(chessGrid,board,"Waiting for Opponent.."));

        initServerClientPause.setOnFinished(event -> {initServerClient(board); if (!hostingSelected) {
            drawField(chessGrid,board,"Waiting for Opponent..");
            networkPause.play();
        }
        else{
            drawField(chessGrid,board,"Connected");
        }
        });
        aiPause.setOnFinished(event -> {
            if(simpleAiSelected){
            input.setInput(simpleAi.getBestMove(board));
        }
        else{
            input.setInput(advancedAi.getBestMove(board));
        }
            if(gameRunning){
                RunGame.run(board,null,-1,input,true);

            }
            drawField(chessGrid,board,"");
        });

        //Creating Elements on the GridPane
        //Label in the left-bottom corner to get a 9x9 grid
        Label blank = new Label();
        blank.setStyle("-fx-font-size: 30;-fx-pref-height: 50px;-fx-pref-width: 50px;");
        chessGrid.add(blank, 0,0);
        //Labels for the bottom letters
        for (int i = 1; i<9;i++){
            Label lbl = new Label();
            lbl.setText("     "+toLetters.charAt(i-1));
            lbl.setStyle("-fx-font-size: 30;-fx-pref-height: 50px;-fx-pref-width: 100px;");
            chessGrid.add(lbl, i,8);
        }
        //Labels for the left numbers
        for (int i = 1; i<9;i++){
            Label lbl = new Label();
            Label lbl1 = new Label();
            lbl.setText("  "+Integer.toString(9-i));
            lbl.setStyle("-fx-font-size: 30;-fx-pref-height: 100px;-fx-pref-width: 50px;");
            lbl1.setStyle("-fx-font-size: 30;-fx-pref-height: 100px;-fx-pref-width: 50px;");
            chessGrid.add(lbl, 0,i-1);
            chessGrid.add(lbl1, 9,i-1);
        }
        //creating all the Buttons for the Field of the Game
        for (int i = 1; i<9; i++){
            for (int j = 1; j<9; j++){

                Button btn = new Button();
                //checks if a Button needs to be silver or white
                if(i%2 == 0 && j%2 == 1 || i%2 == 1 && j%2 == 0){
                    btn.setStyle("-fx-color: silver;-fx-font-size: 35;-fx-pref-height: 100px;-fx-pref-width: 100px;");
                }
                else{
                    btn.setStyle("-fx-font-size: 35;-fx-pref-height: 100px;-fx-pref-width: 100px;");
                }
                //Using the ID feature, all Buttons can be edited later in the Game
                btn.setId(Integer.toString(i-1)+Integer.toString(j-1));
                //A click on the Button will launch the Update field method, giving it the ID to locate the pressed Button
                btn.setOnAction(event -> updateField(chessGrid, board, btn.getId()));
                chessGrid.add(btn, i, j-1);
            }
        }

        //Creating the Settings-Buttons
        Button turnField = new Button();
        Button showPotentialMove = new Button();
        Button selectAgain = new Button();
        Button showInCheck = new Button();
        Button returnToStartScreen = new Button();
        //giving them all the same appearance
        String buttonStyle = "-fx-font-size: 15;-fx-pref-height: 100px;-fx-pref-width: 100px;";
        String buttonStylePressed = "-fx-background-color:gray; -fx-font-size: 15;-fx-pref-height: 100px;-fx-pref-width: 100px;";
        turnField.setStyle(buttonStyle);
        showPotentialMove.setStyle(buttonStyle);
        selectAgain.setStyle(buttonStyle);
        showInCheck.setStyle(buttonStyle);
        returnToStartScreen.setStyle(buttonStyle);
        //giving them their Names als Text fields
        turnField.setText("Turn \n Field");
        showPotentialMove.setText("Show \n Moves");
        selectAgain.setText("Disable \n Reselect");
        showInCheck.setText("Show if \n in Check");
        returnToStartScreen.setText("Return to\n Startscreen");
        //Choosing the action if clicked, mostly a boolean will be set in ChessBoard or the Stage is closed to start a new game
        turnField.setOnAction(actionEvent -> { if(board.isTurnField()){board.setTurnField(false);turnField.setStyle(buttonStyle);}
        else{board.setTurnField(true);turnField.setStyle(buttonStylePressed);}});

        showPotentialMove.setOnAction(actionEvent -> { if(board.isShowMoves()){board.setShowMoves(false);showPotentialMove.setStyle(buttonStyle);}
        else{board.setShowMoves(true);showPotentialMove.setStyle(buttonStylePressed);}});

        selectAgain.setOnAction(actionEvent -> { if(board.isReselect()){board.setReselect(false);selectAgain.setStyle(buttonStyle);}
        else{board.setReselect(true);selectAgain.setStyle(buttonStylePressed);}});

        showInCheck.setOnAction(actionEvent -> { if(board.isShowInCheck()){board.setShowInCheck(false);showInCheck.setStyle(buttonStyle);}
        else{board.setShowInCheck(true);showInCheck.setStyle(buttonStylePressed);}});

        returnToStartScreen.setOnAction(actionEvent ->{if(!localGameSelected){
            printWriter.println("c1-c1");
            printWriter.flush();
        }
        endGame(primaryStage);
        });

        chessGrid.add(turnField,10,0);
        chessGrid.add(showPotentialMove,11,0);
        chessGrid.add(showInCheck,12,0);
        chessGrid.add(selectAgain,13,0);
        chessGrid.add(returnToStartScreen,14,0);

        //Creating the history-label
        String labelStyle = "-fx-font-size: 30;-fx-pref-height: 100px;-fx-pref-width: 500px;";
        Label history = new Label();
        history.setStyle(labelStyle);
        history.setText("History of Moves:");
        chessGrid.add(history,10,1,14,1);
        Label history1 = new Label();
        history1.setStyle("-fx-font-size: 15;-fx-pref-height: 100px;-fx-pref-width: 500px;");
        history1.setText("");
        history1.setId(labelId[0]);
        chessGrid.add(history1,10,1,14,3);

        //Creating the beaten Figure Label
        Label beaten = new Label();
        beaten.setStyle(labelStyle);
        beaten.setText("Beaten Figures:");
        chessGrid.add(beaten,10,2,14,3);
        Label beaten1 = new Label();
        beaten1.setStyle(labelStyle);
        beaten1.setText("");
        beaten1.setId(labelId[1]);
        chessGrid.add(beaten1,10,3,14,3);

        //Creating the Label for displaying Messages
        Label message = new Label();
        message.setStyle(labelStyle);
        message.setText("");
        message.setId(labelId[2]);
        chessGrid.add(message, 10,4,14,5);
        //ChessPane Complete.

        //Creating the initial select Gamemode Stage
        String menuStyle = "-fx-font-size: 20;-fx-pref-height: 200px;-fx-pref-width: 200px;";
        String menuStyleShort = "-fx-font-size: 20;-fx-pref-height: 100px;-fx-pref-width: 200px;";
        String menuStyleSmallFont = "-fx-font-size: 14;-fx-pref-height: 50px;-fx-pref-width: 200px;";
        Label welcome = new Label();
        welcome.setText("Welcome to Chess!");
        welcome.setStyle(menuStyle);
        Label cm = new Label();
        cm.setText("Please choose\nyour Gamemode:");
        cm.setStyle(menuStyle);
        Button pvp = new Button();
        pvp.setOnAction(event -> {primaryStage.setScene(new Scene(networkGame, 400, 400 ));board.setPvp(true);//StartGame.initLineup(board); initField(chessGrid, board);
        });
        pvp.setText("PlayerVSPlayer");
        pvp.setStyle(menuStyleShort);
        Button pvai = new Button();
        pvai.setOnAction(event -> {primaryStage.setScene(new Scene(chooseColor, 400, 400));board.setPvp(false);});
        pvai.setText("PlayerVsAi");
        pvai.setStyle(menuStyleShort);


        chooseMode.add(welcome,0,0);
        chooseMode.add(cm,1,0);
        chooseMode.add(pvp,0,1);
        chooseMode.add(pvai,1,1);
        //select Gamemode Complete.

        //Creating the stage where the Player can play over the network.

        Label networkLabel = new Label();
        networkLabel.setStyle(menuStyle);
        networkLabel.setText("Choose to play local \nor over the Network");
        networkGame.add(networkLabel,0,0);
        Label hostWhite = new Label();
        hostWhite.setStyle(menuStyle);
        hostWhite.setText("The Host will\nbe playing white");
        networkGame.add(hostWhite,1,0);
        hostWhite.setVisible(false);
        ChoiceBox networkSelector = new ChoiceBox(FXCollections.observableArrayList("Local Game", "Play over local Network"));
        networkSelector.getSelectionModel().selectFirst();
        networkSelector.setStyle(menuStyleSmallFont);
        networkGame.add(networkSelector,0,1);
        ChoiceBox hostSelector = new ChoiceBox(FXCollections.observableArrayList("Host a Game", "Join a Game"));
        hostSelector.getSelectionModel().selectFirst();
        hostSelector.setStyle(menuStyleSmallFont);
        networkGame.add(hostSelector,1,1);
        TextField ipAdd = new TextField();
        ipAdd.setText("Please enter Host IP Address");
        ipAdd.setStyle(menuStyleSmallFont);
        networkGame.add(ipAdd,1,2);
        hostSelector.setVisible(false);
        ipAdd.setVisible(false);
        hostSelector.setOnAction(event -> {if(hostSelector.getValue().equals("Join a Game")){
            hostingSelected = false;
            ipAdd.setVisible(true);
        }
        else{hostingSelected = true;
            ipAdd.setVisible(false);}
        });
        networkSelector.setOnAction(event -> {if(networkSelector.getValue().equals("Play over local Network")){
            localGameSelected = false;
            hostSelector.setVisible(true);
            hostWhite.setVisible(true);

        }
        else{localGameSelected = true;
            hostSelector.setVisible(false);
            hostWhite.setVisible(false);
            ipAdd.setVisible(false);
        }
        });

        Button startGame = new Button();
        startGame.setText("Start Game");
        startGame.setStyle(menuStyleSmallFont);
        startGame.setOnAction(event -> {primaryStage.setScene(new Scene(chessGrid, 1400, 850 ));
            if (!ipAdd.getCharacters().toString().equals("Please enter Host IP Address")){
                ipAddress = ipAdd.getCharacters().toString();
            }

            StartGame.initLineup(board);
            initField(chessGrid, board); });
        networkGame.add(startGame,0,2);

        //networkGame done.


        //Creating the PvAI stage that lets the Player choose their color
        Button add = new Button();
        Button sub = new Button();
        String levelButtonStyle = "-fx-font-size: 10;-fx-pref-height: 50px;-fx-pref-width: 50px;";
        add.setStyle(levelButtonStyle);
        sub.setStyle(levelButtonStyle);
        add.setText("+");
        sub.setText("-");
        add.setId("add");
        sub.setId("sub");
        add.setOnAction(event -> {if(advancedAi.getLevel()<3){
            advancedAi.setLevel(advancedAi.getLevel()+1); chooseColor.getChildren().forEach(node ->{
            if( node.getId()!=null &&node.getId().equals(labelId[3])){
                ((Label)node).setText("Number of Levels:  "+ advancedAi.getLevel()+"\n"+difficulty[advancedAi.getLevel()-1]);
            }
        });}});
        sub.setOnAction(event -> {if(advancedAi.getLevel()>1){
            advancedAi.setLevel(advancedAi.getLevel()-1); chooseColor.getChildren().forEach(node ->{
                if(node.getId()!=null &&node.getId().equals(labelId[3])){
                    ((Label)node).setText("Number of Levels: "+ advancedAi.getLevel()+"\n"+difficulty[advancedAi.getLevel()-1]);
                }
            });}});

        ChoiceBox aiSelector = new ChoiceBox(FXCollections.observableArrayList("Simple Ai", "Advanced Ai"));
        aiSelector.getSelectionModel().selectFirst();
        aiSelector.setOnAction(event -> {
            if (aiSelector.getValue() == "Advanced Ai"){
                simpleAiSelected = false;
                chooseColor.getChildren().forEach(node ->{
                    if (node.getId()!=null &&(node.getId().equals("sub") || node.getId().equals("add") || node.getId().equals(labelId[3]))){
                        node.setVisible(true);
                    }
                });
            }
            else{chooseColor.getChildren().forEach(node ->{
                simpleAiSelected = true;
                if (node.getId()!=null &&(node.getId().equals("sub") || node.getId().equals("add") || node.getId().equals(labelId[3]))){
                    node.setVisible(false);
                }
            });

            }
        });

        Label ailevel = new Label();
        ailevel.setId(labelId[3]);
        ailevel.setText("Number of Levels: "+ advancedAi.getLevel()+"\n"+difficulty[0]);
        ailevel.setStyle(menuStyle);
        Label color1 = new Label();
        color1.setText("Player1, choose your \nColor and AI difficulty");
        color1.setStyle(menuStyle);
        Button w = new Button();
        w.setOnAction(event -> {primaryStage.setScene(new Scene(chessGrid, 1400, 850));  simpleAi.setColor(Color.black);
            advancedAi.setColor(Color.black);    StartGame.initLineup(board); initField(chessGrid, board);});
        w.setText("White");
        w.setStyle(menuStyleShort);
        Button b = new Button();
        b.setOnAction(event -> {primaryStage.setScene(new Scene(chessGrid, 1400, 850));  simpleAi.setColor(Color.white);
            advancedAi.setColor(Color.white); board.setLastMoveValid(true);   StartGame.initLineup(board); initField(chessGrid, board);});
        b.setText("Black");
        b.setStyle(menuStyleShort);
        chooseColor.add(color1,1,0);
        chooseColor.add(aiSelector,1,1);
        chooseColor.addRow(3,b,w);
        chooseColor.add(ailevel,0,0,1,4);
        chooseColor.add(add,1,2);
        chooseColor.add(sub,0,2);
        add.setVisible(false);
        sub.setVisible(false);
        ailevel.setVisible(false);
        //chooseColor Complete.

        //selecting the initial Gamemode select scene and Displaying it
        primaryStage.setScene(new Scene(chooseMode, 400, 400));
        primaryStage.show();

    }


    /**
     * The methode used to initially display the Chessboard before any Move has been made on it
     *
     *
     * @param chessGrid The GridPane used to build the Chessfield on.
     * @param board The ChessBoard the game will be running on.
     */
    private void initField(GridPane chessGrid, ChessBoard board){

        chessGrid.getChildren().forEach(node ->{

            if (node.getId() != null && !node.getId().equals(labelId[0]) && !node.getId().equals(labelId[1]) && !node.getId().equals(labelId[2])){

                int x = Integer.parseInt(String.valueOf(node.getId().charAt(0)));
                int y = Integer.parseInt(String.valueOf(node.getId().charAt(1)));

                if (board.getFigure(x,y) != null){
                    ((Button) node).setText(board.getFigure(x,y).getUnicode());
                }
            }
            
        }  );
        //AI has to make the first move
        if(!board.isPvp() && board.isLastMoveValid()){
            aiPause.play();
            /*
            if(simpleAiSelected){
                input.setInput(simpleAi.getBestMove(board));
            }
            else{
                input.setInput(advancedAi.getBestMove(board));
            }
            if(gameRunning){
                RunGame.run(board,null,-1,input,true);
            }
*/

            chessGrid.getChildren().forEach(node ->{
            	boolean basic=node.getId() != null && !node.getId().equals(labelId[0]) && !node.getId().equals(labelId[1]) && !node.getId().equals(labelId[2]);
                if (basic){

                    int x = Integer.parseInt(String.valueOf(node.getId().charAt(0)));
                    int y = Integer.parseInt(String.valueOf(node.getId().charAt(1)));

                    if (board.getFigure(x,y) != null){
                        ((Button) node).setText(board.getFigure(x,y).getUnicode());
                    }
                    else{
                        ((Button) node).setText("");
                    }
                }
            }  );
        }
        if(!localGameSelected) {
            initFieldPause.play();
            initServerClientPause.play();


        }

    }

    /**
     * This method is called on every click on one of the Tiles in the Chessfield. Here, all the decisions on what to display after each
     * click and each move are made. Also the communication with the RunGame Class is used for Gameplay decisions by pushing the changed ChessBoard
     * as an Input like with the cli to the run() and pulling the changed Chessboard form there.
     *
     *
     * @param chessGrid The GridPane used to build the Chessfield on
     * @param board The ChessBoard the game will be running on.
     * @param id The Button ID that triggered the call of this method
     */


    private void updateField(GridPane chessGrid, ChessBoard board, String id) {
        //Here it is important to decide if the Button is pressed the first time, so a Figure is selected,
        //or if it is pressed a second time so a field to move on is selected
        if (board.getSelectedField().equals("")){
            board.setSelectedField(id);
            updateFieldFirstClick(chessGrid,board,id);
        }

        else{
            //Second time a Field is selected, a move can be made
            //Converting the selected Field and the one before to a Input like in the cli
            //Adjusting here for a rotated Field
            String inputStr = String.valueOf(toLetters.charAt(Integer.parseInt(String.valueOf(board.getSelectedField().charAt(0)))))
                    +String.valueOf(8-Integer.parseInt(String.valueOf(board.getSelectedField().charAt(1))))+"-"+String.valueOf(toLetters.charAt(Integer.parseInt(String.valueOf(id.charAt(0)))))
                    +String.valueOf(8-Integer.parseInt(String.valueOf(id.charAt(1))));
            if (board.isTurnField() && input.getActivePlayer() ==2){
                inputStr = String.valueOf(toLetters.charAt(7-Integer.parseInt(String.valueOf(board.getSelectedField().charAt(0)))))
                        +String.valueOf(1+Integer.parseInt(String.valueOf(board.getSelectedField().charAt(1))))+"-"+String.valueOf(toLetters.charAt(7-Integer.parseInt(String.valueOf(id.charAt(0)))))
                        +String.valueOf(1+Integer.parseInt(String.valueOf(id.charAt(1))));
            }
            inputStr = inputStr.toLowerCase();
            input.setInput(inputStr);
            //Here the run() method is called to handle Gameplay decisions and if the move is valid
            if (gameRunning){
                RunGame.run(board,null,-1,input,true);
            }
            //Updated ChessBoard is Drawn
            drawField(chessGrid,board,"");
            updateFieldSecondClick(board,inputStr);
        }
    }

    /**
     * This methode handles the Gui elements for the first Click a player makes on a Field and is called by updateField()
     * @param chessGrid The GridPane used to build the Chessfield on
     * @param board The ChessBoard the game will be running on.
     * @param id The Button ID that triggered the call of this method
     */
    private void updateFieldFirstClick(GridPane chessGrid, ChessBoard board, String id) {
        //Since Java has a problem with Variables that ware not final in Lambda Expressions,
        //but here the Variable might change if the Field should rotate, this helps avoids errors
        int xAvoidLambdaFinalError;
        int yAvoidLambdaFinalError;
        if (!board.isTurnField()||input.getActivePlayer() ==1){

            xAvoidLambdaFinalError = Integer.parseInt(String.valueOf(id.charAt(0)));
            yAvoidLambdaFinalError = Integer.parseInt(String.valueOf(id.charAt(1)));
        }
        else{
            xAvoidLambdaFinalError = 7-Integer.parseInt(String.valueOf(id.charAt(0)));
            yAvoidLambdaFinalError = 7-Integer.parseInt(String.valueOf(id.charAt(1)));
        }

        //The ID of the clicked Button as Integer, adjusted for a rotated Field
        int xId = xAvoidLambdaFinalError;
        int yId = yAvoidLambdaFinalError;

        chessGrid.getChildren().forEach(node -> {
            if (node.getId() != null && node.getId().charAt(0) == id.charAt(0) && node.getId().charAt(1) == id.charAt(1)){
                ((Button) node).setText(((Button) node).getText()+"*");

            }
        });




        if(board.isShowMoves() && board.getFigure(xId,yId) != null){

            //Getting the Potential Moves of a Figure for Displaying of the Option is selected

            boolean[][] moveArr = new boolean[8][8];
            ChessBoard copy = RunGame.boardcopy(board);


            for (int i = 0 ; i < 8; i++){
                for (int j = 0; j < 8; j++){

                    Input testInput = new Input();
                    testInput.setInput(SimpleAI.transformToInput(xId,yId,i,j));
                    boolean testIfValid = Rules.noRuleBroken(copy, testInput);
                    moveArr[i][j] = testIfValid;


                }
            }

            //Marking all the available  Fields with an "X"
            chessGrid.getChildren().forEach(node -> {
                if (node.getId() != null && !node.getId().equals(labelId[0]) && !node.getId().equals(labelId[1]) && !node.getId().equals(labelId[2]) && (board.getFigure(xId, yId).getColor()==Color.white && 1 ==input.getActivePlayer()
                        || board.getFigure(xId, yId).getColor()==Color.black && 2 ==input.getActivePlayer())){

                    int x = Integer.parseInt(String.valueOf(node.getId().charAt(0)));
                    int y = Integer.parseInt(String.valueOf(node.getId().charAt(1)));

                    if (moveArr[x][y] && (!board.isTurnField()||input.getActivePlayer() ==1) ){

                        ((Button) node).setText(((Button) node).getText() + "x");
                    }
                    else if (moveArr[7-x][7-y] && board.isTurnField() && input.getActivePlayer() ==2){
                        ((Button) node).setText(((Button) node).getText() + "x");
                    }

                }
            }  );
        }
    }

    /**
     * This methode handles the Gui elements for the second Click a player makes on a Field and is called by updateField()
     * @param board  The ChessBoard the game will be running on.
     * @param inputStr The String that stores the desired Moves to make
     */
    private void updateFieldSecondClick(ChessBoard board, String inputStr) {
        //The first selected Field is cleared, but if reselection is disabled and a Figure was selected
        //while the Move was invalid, it keeps this Figure selected for the next click, adjusted for rotation
        if (board.isReselect() && (!board.isTurnField()|| input.getActivePlayer() == 1)){
            int xId = Integer.parseInt(String.valueOf(board.getSelectedField().charAt(0)));
            int yId = Integer.parseInt(String.valueOf(board.getSelectedField().charAt(1)));
            boolean right=1==input.getActivePlayer();
            if (board.isLastMoveValid() || board.getFigure(xId,yId) == null || board.getFigure(xId, yId).getColor()==Color.white && 2 ==input.getActivePlayer()
                    || board.getFigure(xId, yId).getColor()==Color.black && right){
                board.setSelectedField("");
            }
        }
        else if (board.isReselect() && board.isTurnField() && input.getActivePlayer() ==2){
            int xId = 7-Integer.parseInt(String.valueOf(board.getSelectedField().charAt(0)));
            int yId = 7-Integer.parseInt(String.valueOf(board.getSelectedField().charAt(1)));
            if (board.isLastMoveValid() || board.getFigure(xId,yId) == null || board.getFigure(xId, yId).getColor()==Color.white && 2 ==input.getActivePlayer()
                    || board.getFigure(xId, yId).getColor()==Color.black && 1 ==input.getActivePlayer()){
                board.setSelectedField("");
            }
        }
        else{
            board.setSelectedField("");
        }
        if(!board.isPvp() && board.isLastMoveValid() && gameRunning){
            waitForOpponentPause.play();
            aiPause.play();
        }
        if (!localGameSelected && board.isLastMoveValid() && gameRunning){
            printWriter.println(inputStr);
            printWriter.flush();
            waitForOpponentPause.play();
            networkPause.play();
        }
    }

    /**
     * This method is used multiple times to simply redraw an updated version of the Field by not only
     * placing every figure, but also update things like the messageField or beatenFiguresField.
     *
     @param chessGrid The GridPane used to build the Chessfield on.
      * @param board The ChessBoard the game will be running on.
     */
    private void drawField(GridPane chessGrid, ChessBoard board, String message){

        chessGrid.getChildren().forEach(node ->{

            if (node.getId() != null && !(board.isReselect() && !board.isLastMoveValid())&&!node.getId().equals(labelId[0])
                    && !node.getId().equals(labelId[1]) && !node.getId().equals(labelId[2])){

                int x = Integer.parseInt(String.valueOf(node.getId().charAt(0)));
                int y = Integer.parseInt(String.valueOf(node.getId().charAt(1)));
                //If Rotation is enabled, the Figures will be flipped
                if (board.isTurnField() &&  input.getActivePlayer() == 2){
                    if (board.getFigure(7-x,7-y) != null){
                        ((Button) node).setText(board.getFigure(7-x,7-y).getUnicode());
                    }
                    else{
                        ((Button) node).setText("");
                    }
                }

                else{
                    if (board.getFigure(x,y) != null){
                        ((Button) node).setText(board.getFigure(x,y).getUnicode());
                    }
                    else{
                        ((Button) node).setText("");
                    }
                }



            }
            // The Game History is updated
            else if (node.getId() != null && Objects.equals(node.getId(), labelId[0])){

                StringBuilder arrToStr = new StringBuilder();

                for (String s : board.getMovedList())
                {
                    arrToStr.append("[").append(s).append("] ");
                }
                if (arrToStr.length() > 72){
                    arrToStr = new StringBuilder(arrToStr.substring(arrToStr.length() - 72, arrToStr.length()));
                }


                ((Label) node).setText(arrToStr.toString());
            }
            //The Beaten Figure list is updated
            else if (node.getId() != null && node.getId().equals(labelId[1])){

                StringBuilder arrToStr = new StringBuilder();
                boolean newline = true;
                String temp;
                for (String s : board.getBeatenList())
                {
                    temp = board.toUTF8(s);

                    arrToStr.append(temp).append(" ");
                    if(newline && arrToStr.length()>22){
                        newline = false;
                        arrToStr.append("\n");
                    }

                }
                ((Label) node).setText(arrToStr.toString());

            }
            //The Field for displaying messages is updated
            else if (node.getId() != null && node.getId().equals(labelId[2])){

                if(Rules.gameFinished(board,input.getActivePlayerColor())) {
                    gameRunning = false;
                    if (board.isDraw()) {
                        ((Label) node).setText("!Draw");
                    }
                    else if (input.getActivePlayerColor()==Color.white){
                        ((Label) node).setText("!Black wins");
                    }
                    else{
                        ((Label) node).setText("!White wins");
                    }
                }
                else if (board.isShowInCheck() && Rules.scanBoardForCheck(board)[0]){
                    ((Label) node).setText("!King in Check detected");
                }
                else {

                    ((Label) node).setText(message);
                }

            }
        }  );
    }

    /**
     * This method is used for over the Network play, it will check if the player wants to Host or Join a Game
     * and than configure the Socket and ServerSocket in the correct way. Also, it will only finish if both players
     * are Connected, therefore making sure that the Game will only start after a successful connection.
     * @param board  The ChessBoard the game will be running on.
     */
    private void initServerClient(ChessBoard board){
        if(hostingSelected){

            try {
                server = new ServerSocket(7777);
                System.out.println("Waiting for Client to Connect..");
                client = server.accept();
                System.out.println("Connected!");
                inputStreamReader = new InputStreamReader(client.getInputStream());
                bufferedReader = new BufferedReader(inputStreamReader);
                printWriter = new PrintWriter(client.getOutputStream());


            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else{

            try {
                client = new Socket(ipAddress,7777);
                inputStreamReader = new InputStreamReader(client.getInputStream());
                bufferedReader = new BufferedReader(inputStreamReader);
                printWriter = new PrintWriter(client.getOutputStream());
                board.setLastMoveValid(true);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    private void endGame(Stage primaryStage){
        if(hostingSelected && !localGameSelected){
            try {
                server.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        gameRunning = true;
        simpleAiSelected = true;
        hostingSelected = true;
        gameRunning = true;
        input.setActivePlayer(1);
        primaryStage.close();
        start(primaryStage);
    }

    /**
     * The entry point of the GUI application.
     *
     * @param args The command line arguments passed to the application
     */
    public static void main(String[] args) {
        launch(args);
    }


    }




