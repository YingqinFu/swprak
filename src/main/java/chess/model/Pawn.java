package chess.model;

import chess.controller.Input;

/**
	 * the pawn move
	 * @author Yingqin Fu, Steffen Häusler, Christoph Cohrs-Thiede from Gruppe 20
	 *
	 */
public class Pawn extends Figure{
	
	/**
	 * the current count of pawn move
	 */
	
	/**
	 * Constructor. Calls super constructor.
	 * @param x x-coodi
	 * @param y y-coodi
	 * @param color the color of users
	 * @param potentialMove the potential move of figures
	 */
	public Pawn(int x,int y,Color color,boolean[][] potentialMove) {
		super(x,y,color,potentialMove);
		this.points = 1;
	}


	/**
	 * get-method of board name
	 * if color is "w", board name is in capital letter
	 * if color is "b" board name is in lower case letter
	 * @return name boardVisual of the pawn as a String
	 */
	@Override
	public String getName() {
		return this.getColor() == Color.white ? "P" : "p";
		
	}

	
	/**
	 * get-methode for the Unicode used in the 2D-Gui
	 * @return the unicode symbol of the Figure, depending on its color.
	 */
	@Override
	public String getUnicode() {
		return this.getColor() == Color.white ? "♙" : "♟";
	}
	
	/**
	 * the function to check,if the movement of pawn is first time or not 
	 * @param x x-coodi
	 * @param y y-coodi
	 * @return firsTimeMove is there first move or not
	 */
	public boolean firstMove(int x,int y ) {
		boolean firsTimeMove=false;
		if(y==1&&this.getColor()==Color.black) {
			firsTimeMove=true;
		}
		 if(y==6&&this.getColor()==Color.white) {
			 firsTimeMove=true;
		}
		
		return firsTimeMove;
		
		
	}
	
	/**
	 * @param x axis position, y axis position
	 * Unlike the other pieces, pawns cannot move backwards. Normally a pawn moves by advancing a single square, 
	 * but the first time a pawn moves, it has the option of advancing two squares.
	 * Pawns may not use the initial two-square advance to jump over an occupied square, or to capture. 
	 *  Any piece immediately in front of a pawn, friend or foe, blocks its advance. 
	 */
	@Override
	public void setPotentialMove(int x,int y,ChessBoard board){
	
		for (int j = 0; j < 8; j++){
			for (int k = 0; k < 8; k++){
				this.potentialMove[j][k] = false;
			}
		}
		
		//set the potential move of white pawn
		setWhitePawn(x,y,board);
		
		
		//set the potential move of black pawn
		setBlackPawn(x,y,board);
		
		
	}
	
	/**
	 * set the potential move of white pawn
	 * @param x x-axis
	 * @param y y-axis
	 * @param board the current board
	 */
	public void setWhitePawn(int x,int y,ChessBoard board) {
		
		if (this.getColor()==Color.white ){
			//if it is the first move of pawn,it has the option of advancing two squares or one square. 
			if(firstMove(x,y)) {
				if(board.getFigure(x, y-1)==null) {
					this.potentialMove[x][y-1] = true;
				}
			
				if(board.getFigure(x, y-2)==null&&board.getFigure(x, y-1)==null) {
				
					this.potentialMove[x][y-2] = true;
				}
				//call the capture rules of white pawn
				whitePawnCapture(x,y,board);
			
			
			
			}
				//if it is NOT the first move of pawn,it can only move one square forward. 
			else {
			
				if(y-1>=0&&board.getFigure(x, y-1)==null) {
					this.potentialMove[x][y-1] = true;
				}
				
			//call the capture rules of white pawn
				whitePawnCapture(x,y,board);
		
	       } 
	  }
		
	}
	
	/**
	 * set the potential move of black pawn
	 * @param x x-axis
	 * @param y y-axis
	 * @param board the current board
	 */
	public void setBlackPawn(int x,int y,ChessBoard board) {
		
		if(this.getColor()==Color.black){
			if(firstMove(x,y)) {
				
				if(board.getFigure(x, y+1)==null) {
				this.potentialMove[x][y+1] = true;
				}
				
				if(board.getFigure(x, y+2)==null&&board.getFigure(x, y+1)==null) {
				this.potentialMove[x][y+2] = true;
				}
			//call the capture rules of black pawn
				blackPawnCapture(x,y,board);
				
		 
			}
			else {
			 // if(y+1<8) {
				if(y+1<8&&board.getFigure(x, y+1)==null) {
					this.potentialMove[x][y+1] = true;
				}
				
			//call the capture rules of black pawn
				blackPawnCapture(x,y,board);
				
			
				
			//}
		  }
		}
		
	}
	
	
	/**
	 * the rules for capture of white pawn
	 * @param x x-coodi
	 * @param y y-coodi
	 * @param board the current board
	 */
	
	public void whitePawnCapture(int x,int y,ChessBoard board) {
				
			if(x-1>=0&&y-1>=0&&board.getFigure(x-1, y-1)!=null&&board.getFigure(x-1, y-1).getColor()==Color.black) {
					
						
						this.potentialMove[x-1][y-1]=true;
					
			}
			
			if(x+1<8&&y-1>=0&&board.getFigure(x+1, y-1)!=null&&board.getFigure(x+1, y-1).getColor()==Color.black) {
					
						this.potentialMove[x+1][y-1] = true;
						
			}
				
			
		
	}
	/**
	 * the rules for capture of black pawn
	 * @param x x-coodi
	 * @param y y-coodi
	 * @param board the current board
	 */
	public void blackPawnCapture(int x,int y,ChessBoard board) {
				
		    if(x-1>=0&&y+1<8&&board.getFigure(x-1, y+1)!=null&&board.getFigure(x-1, y+1).getColor()==Color.white) {				
						this.potentialMove[x-1][y+1]=true;
				
		    }
		    
		    if(x+1<8&&y+1<8&&board.getFigure(x+1, y+1)!=null&&board.getFigure(x+1, y+1).getColor()==Color.white) {

						this.potentialMove[x+1][y+1] = true;
				
		    }
			
	}
	
	/**
	 * the rules for capture of white pawn.when En Passant happens
	 * @param input the input of users
	 * @param board the current board
	 */
	@Override
	public void whiteEnPassant(Input input,ChessBoard board) {
		
		//first to check,whether the white pawn at the correct position and the black pawn finish its first move with two squares forward or not
		if(!board.getMovedList().isEmpty()&&this.getName().equals("P")&&Input.changeInputToCoordinate(input.getInput().charAt(1))==3
				&&board.getMovedList().get(board.getMovedList().size()-1).charAt(1)=='7'
				&&board.getMovedList().get(board.getMovedList().size()-1).charAt(4)=='5') {
			
			
			//the black pawn at the left side of the current white pawn 
			leftWhiteEnPassant(input,board);
			//the black pawn at the right side of the current white pawn 
			
			rightWhiteEnPassant(input,board);
			
			
		}
		
		
	}

	/**
	 * the rules for capture of black pawn.when En Passant happens
	 * @param input the input of users
	 * @param board the current board
	 */
	@Override
	public void blackEnPassant(Input input,ChessBoard board) {
		
    	
    
		//first to check,whether the black pawn at the correct position and the white pawn finish its first move with two squares forward or not
		if(!board.getMovedList().isEmpty()&&this.getName().equals("p")&&Input.changeInputToCoordinate(input.getInput().charAt(1))==4
				&&board.getMovedList().get(board.getMovedList().size()-1).charAt(1)=='2'
				&&board.getMovedList().get(board.getMovedList().size()-1).charAt(4)=='4') {
		
			
			//the white pawn at the left side of the current black pawn 
			leftBlackEnPassant(input,board);
			//the white pawn at the right side of the current black pawn 
			rightBlackEnPassant(input,board);
			
		}
		
	}

	
	
	
	
	
}
