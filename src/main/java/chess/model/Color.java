package chess.model;

/**
 * Enumeration of the two color, black and white.
 * @author Yingqin Fu, Steffen Häusler, Christoph Cohrs-Thiede from Gruppe 20 
 *
 */
	public enum Color {
		white,
		black
	}
