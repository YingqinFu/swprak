package chess.model;
    /**
     * the knight class
     * @author Yingqin Fu, Steffen Häusler, Christoph Cohrs-Thiede from Gruppe 20
     *
     */
public class Knight extends Figure{
	/**
	 * Constructor. Calls super constructor.
	 * @param x x-coodi
	 * @param y y-coodi
	 * @param color the color of figures
	 * @param potentialMove the potential move of users
	 */
	public Knight(int x,int y,Color color,boolean[][] potentialMove) {
		super(x,y,color,potentialMove);
		this.points = 3;
	}
	
	/**
	 * get-method of board name
	 * if color is "w", board name is in capital letter
	 * if color is "b", board name is in lower case letter
	 * @return boardVisual of the knight as a String
	 */
	@Override
	public String getName() {
		return this.getColor() == Color.white ? "N" : "n";
	}
	
	/**
	 * get-methode for the Unicode used in the 2D-Gui
	 * @return the unicode symbol of the Figure, depending on its color.
	 */
	@Override
	public String getUnicode() {
		return this.getColor() == Color.white ? "♘" : "♞";

	}
	/**
	 * @param x axis position, y axis position
	 * it may move two squares vertically and one square horizontally, or two squares horizontally and one square vertically 
	 * (with both forming the shape of an L). This way, a knight can have a maximum of 8 moves. 
	 * While moving, the knight can jump over pieces to reach its destination.
	 */
	@Override
	public void setPotentialMove(int x,int y,ChessBoard board){
		for (int j = 0; j < 8; j++){
			for (int k = 0; k < 8; k++){
				super.potentialMove[j][k] = false;
			}
		}
		//set the null position as true, and the enemy position as true
		setPosSpe(x, y, board);
		
		
	}
	
	
	/**
	 * set the null position as true
	 * @param x x-axis
	 * @param y y-axis
	 * @param board the current board
	 */
	public void setPosSpe(int x, int y, ChessBoard board) {
		
		int [] xarr = {x+1,x+2,x+2,x+1,x-1,x-2,x-2,x-1};
		int [] yarr = {y+2,y+1,y-1,y-2,y-2,y-1,y+1,y+2};
		
		for(int i = 0; i < 8; i++){
			
			
				if(xarr[i]>=0 && xarr[i]<8 && yarr[i]>=0 && yarr[i]<8&&(board.getFigure(xarr[i], yarr[i])==null
						||board.getFigure(xarr[i], yarr[i])!=null&&board.getFigure(xarr[i], yarr[i]).getColor()!=
						 board.getFigure(x, y).getColor())) {
					
					super.potentialMove[xarr[i]][yarr[i]] = true;
				
			
				
			}
	
	    }
	}
	
	
	
}
