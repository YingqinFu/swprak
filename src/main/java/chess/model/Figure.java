package chess.model;

import chess.controller.Input;

/**
	 * the class figure
	 * @author Yingqin Fu, Steffen Häusler, Christoph Cohrs-Thiede from Gruppe 20
	 *
	 */
public class Figure {

	/**
	 * 2D-Array of boolean for the potential move
	 */
	protected boolean[][] potentialMove;
	/**
	* x axis position
	*/
	protected int x;
	
	/**
	* y axis position
	*/
	protected int y;
	/**
	* name's board visualization
	*/
	protected String name;
	
	/**
	* The name's color
	*/
	private Color color;
	/**
	 * the points
	 */
	protected int points;
	
	/**
	 * to store the array
	 * @param potentialMove the 2d-array
	 * @param copy the 2d-array to be stored
	 */
	public void copyPotentialMove(boolean[][] potentialMove,boolean[][] copy) {
		
		for (int j = 0; j < 8; j++){
			for (int k = 0; k < 8; k++){
				copy[j][k]=potentialMove[j][k];
			}
		}
		
	}
	
	/**
	 * the constructor of Figure
	 * @param x x-axis
	 * @param y y-axis
	 * @param color the color of figures
	 * @param potentialMove  the potential move of figures
	 */
	public Figure(int x,int y,Color color, boolean[][] potentialMove) {
		
		boolean [][] copy=new boolean[8][8];
		copyPotentialMove(potentialMove,copy);
		this.potentialMove =copy;
		this.x=x;
		this.y=y;
		this.color=color;
	}
	
	
	/**
	 * set-method of potential move
	 * @param x x-axis
	 * @param y y-axis
	 * @param board the current board
	 */
	public void setPotentialMove(int x,int y,ChessBoard board) { 
		
		
	}
	
	/**
	 * get-method of pos1
	 * @return pos1 x axis position
	 */
	public int getX() {
		return x; 
	}
	
	/**
	 * get-method of pos2
	 * @return y axis position
	 */
	public int getY() {
		return y; 
	}
	
	/**
	 * get-method of boardVisual
	 * @return boardVisual of the chess as a String
	 */
	public String getName() {
	
		return name;
	}
	/**
	 * get-method of the type
	 * @return type of the name
	 */
	/**
	 * get-method of color
	 * @return color as a String
	 */
	public Color getColor() {
		return color;
	}
	/**
	 * get-method of potential move
	 * @return potentialMove the potential move of figures
	 */
	public boolean[][] getPotentialMove() {
		
		
		
		boolean [][] copy=new boolean[8][8];
		
		copyPotentialMove(this.potentialMove,copy);
		
		return copy;
		
	}
	/**
	 * the getter of points
	 * @return points the points of figures
	 */
	public int getPoints() {
		return points;
	}



	/**
	 * the rules for the promotion of white pawn
	 * @param input the input of users
	 * @param board the current board
	 */
	public void whitePromotion(Input input,ChessBoard board) {
		//first of all,check whether a white pawn is going to the eighth rank or not
    	if(this.getName().equals("P")&&input.getInput().charAt(1)=='7'
    			&&input.getInput().charAt(4)=='8') {
    		
    		//then check whether the users enter a promotion figures or not
    		setWhitePromotion(input,board);
    	}
	
	}
	
	
	/**
	 * the rules for the promotion of black pawn
	 * @param input the input of users
	 * @param board the currant users
	 */
	public void blackPromotion(Input input,ChessBoard board) {
		//first of all,check whether a white pawn is going to the eighth rank or not
    	if(this.getName().equals("p")&&input.getInput().charAt(1)=='2'
    			&&input.getInput().charAt(4)=='1') {
    		
    		//set the new figures
    		setBlackPromotion(input,board);
    		
    		
    		
    	}
		
	}
	
	

	/**
	 * set the new figures
	 * @param input the input of users
	 * @param board the current board
	 */
	public void setWhitePromotion(Input input,ChessBoard board) {
		int targetX=Input.changeInputToCoordinate(input.getInput().charAt(3));
    	int targetY=Input.changeInputToCoordinate(input.getInput().charAt(4));
    	
    	if(input.getInput().length()==6&&!Input.invalidInputWithPromotion(input)) {
		//Create the corresponding figures at the target position
		switch(input.getInput().charAt(5)) {
		case 'Q':
			board.setFigure(targetX, targetY, new Queen(targetX,targetY,Color.white,potentialMove));
			break;
		
		case'R':
			board.setFigure(targetX, targetY, new Rook(targetX,targetY,Color.white,potentialMove));
			break;
			
		case'B':
			board.setFigure(targetX, targetY, new Bishop(targetX,targetY,Color.white,potentialMove));
			break;
			
		case'N':
			board.setFigure(targetX, targetY, new Knight(targetX,targetY,Color.white,potentialMove));
			break;
			
		default:
			break;

		    }
	    }
    	//if the users didn't enter a promotion figure,then the promotion of pawn will be automatic a Queen figure.
    	else {
			
			board.setFigure(targetX, targetY, new Queen(targetX,targetY,Color.white,potentialMove));
		}
	}
	
	
	/**
	 * set the new figures
	 * @param input the input of users
	 * @param board the current board
	 */
	public void setBlackPromotion(Input input,ChessBoard board) {
		int targetX=Input.changeInputToCoordinate(input.getInput().charAt(3));
    	int targetY=Input.changeInputToCoordinate(input.getInput().charAt(4));
    	
    	if(input.getInput().length()==6&&!Input.invalidInputWithPromotion(input)) {
		//Create the corresponding figures at the target position
		switch(input.getInput().charAt(5)) {
		case 'Q':
			board.setFigure(targetX, targetY, new Queen(targetX,targetY,Color.black,potentialMove));
			break;
		
		case'R':
			board.setFigure(targetX, targetY, new Rook(targetX,targetY,Color.black,potentialMove));
			break;
			
		case'B':
			board.setFigure(targetX, targetY, new Bishop(targetX,targetY,Color.black,potentialMove));
			break;
			
		case'N':
			board.setFigure(targetX, targetY, new Knight(targetX,targetY,Color.black,potentialMove));
			break;
			
		default:
			break;

		    }
	    }
    	
    	//if the users didn't enter a promotion figure,then the promotion of pawn will be automatic a Queen figure.
    	else {
			
			board.setFigure(targetX, targetY, new Queen(targetX,targetY,Color.black,potentialMove));
		}
	}
		
	
	/**
	 * the method to check,whether a figure has already moved or not
	 * @param board the current board
	 * @return hasMoved the figures has moved or not
	 */
	public boolean hasMoved(ChessBoard board) {
		boolean hasMove=false;
		//check all the move in ArrayList,whether it contains the same source position or not
		for (String i : board.getMovedList()) {
			int sourceX=Input.changeInputToCoordinate(i.charAt(0));
	    	int sourceY=Input.changeInputToCoordinate(i.charAt(1));	
	    	
	    	if(this.getX()==sourceX&&this.getY()==sourceY) {
	    		hasMove=true;
	    		
	    	}
        }
		
		
		return hasMove;
		
	}
	/**
	 * the castling move of white
	 * @param input the input of users
	 * @param board the current board
	 */
	public void whiteCastling(Input input ,ChessBoard board) {
		
	}
	
	/**
	 * the castling move of black
	 * @param input the input of users
	 * @param board the current board
	 */
	public void blackCastling(Input input,ChessBoard board) {
		
	}
	/**
	 * En passant of black pawn
	 * @param input the input of users
	 * @param board the current board
	 */
	public void blackEnPassant(Input input,ChessBoard board) {
		
	}
	/**
	 * En passant of white pawn
	 * @param input the input of users
	 * @param board the current board
	 */
	public void whiteEnPassant(Input input,ChessBoard board) {
		
	}
	
	

	/**
	 * the black pawn at the left side of the current white pawn
	 * @param input the input of users
	 * @param board the current board
	 */
	public void leftWhiteEnPassant(Input input,ChessBoard board) {

		int sourceX=Input.changeInputToCoordinate(input.getInput().charAt(0));
    	int sourceY=Input.changeInputToCoordinate(input.getInput().charAt(1));	
    	int targetX=Input.changeInputToCoordinate(input.getInput().charAt(3));
    	int targetY=Input.changeInputToCoordinate(input.getInput().charAt(4));
    	
    	
    	boolean b=Input.changeInputToCoordinate(board.getMovedList().get(board.getMovedList().size()-1).charAt(0))==sourceX-1
				&&Input.changeInputToCoordinate(board.getMovedList().get(board.getMovedList().size()-1).charAt(3))==sourceX-1
				&&board.getFigure(sourceX-1, sourceY)!=null
				&&board.getFigure(sourceX-1, sourceY).getName().equals("p");
    	if(sourceX-1>=0&&b) {
			
			
			//set the position of black pawn to null
			if(targetX-sourceX==-1&&targetY-sourceY==-1) {
				 board.getBeatenList().add(board.getFigure(sourceX-1, sourceY).getName());
				board.setFigureNull(sourceX-1, sourceY);
				
			}
			
			this.potentialMove[sourceX-1][sourceY-1]=true;
			}
    	
    
		
	}
	
	/**
	 * the black pawn at the right side of the current white pawn
	 * @param input the input of users
	 * @param board the current board
	 */
	public void rightWhiteEnPassant(Input input,ChessBoard board) {
		
		int sourceX=Input.changeInputToCoordinate(input.getInput().charAt(0));
    	int sourceY=Input.changeInputToCoordinate(input.getInput().charAt(1));	
    	int targetX=Input.changeInputToCoordinate(input.getInput().charAt(3));
    	int targetY=Input.changeInputToCoordinate(input.getInput().charAt(4));
		
    	//the black pawn at the right side of the current white pawn
    	boolean b =sourceX+1<8&&Input.changeInputToCoordinate(board.getMovedList().get(board.getMovedList().size()-1).charAt(0))==sourceX+1
				&&Input.changeInputToCoordinate(board.getMovedList().get(board.getMovedList().size()-1).charAt(3))==sourceX+1;
    	
		if(b&&board.getFigure(sourceX+1, sourceY)!=null
				&&board.getFigure(sourceX+1, sourceY).getName().equals("p")) {
			
		    //set the position of black pawn to null
			if(targetX-sourceX==1&&targetY-sourceY==-1) {
				// board.getBeatenList().add(board.getFigure(sourceX+1, sourceY).getName());
				board.setFigureNull(sourceX+1, sourceY);
			}
			this.potentialMove[sourceX+1][sourceY-1]=true;
		
			
		
			
			}
		
	}
	
	
	
	/**
	 * the black pawn at the left side of the current white pawn 
	 * @param input the input of users
	 * @param board the current board
	 */
	public void leftBlackEnPassant(Input input,ChessBoard board) {
		int sourceX=Input.changeInputToCoordinate(input.getInput().charAt(0));
    	int sourceY=Input.changeInputToCoordinate(input.getInput().charAt(1));	
    	int targetX=Input.changeInputToCoordinate(input.getInput().charAt(3));
    	int targetY=Input.changeInputToCoordinate(input.getInput().charAt(4));
    	
    	boolean basic=sourceX-1>=0&&Input.changeInputToCoordinate(board.getMovedList().get(board.getMovedList().size()-1).charAt(0))==sourceX-1
				&&Input.changeInputToCoordinate(board.getMovedList().get(board.getMovedList().size()-1).charAt(3))==sourceX-1;
    	boolean lb=sourceX-1>=0&&board.getFigure(sourceX-1, sourceY)!=null
				&&board.getFigure(sourceX-1, sourceY).getName().equals("P");
    	
    	if(basic&&lb) {
			
			
		//set the position of white pawn to null	
			if(targetX-sourceX==-1&&targetY-sourceY==1) {
				board.setFigureNull(sourceX-1, sourceY);
			}
			
			
			this.potentialMove[sourceX-1][sourceY+1]=true;
			
			
			}
		
	}
	/**
	 * the black pawn at the right side of the current white pawn 
	 * @param input the input of users
	 * @param board the current board
	 */
	public void rightBlackEnPassant(Input input,ChessBoard board) {
		int sourceX=Input.changeInputToCoordinate(input.getInput().charAt(0));
    	int sourceY=Input.changeInputToCoordinate(input.getInput().charAt(1));	
    	int targetX=Input.changeInputToCoordinate(input.getInput().charAt(3));
    	int targetY=Input.changeInputToCoordinate(input.getInput().charAt(4));
    	
    	
    	if(sourceX+1<8&&Input.changeInputToCoordinate(board.getMovedList().get(board.getMovedList().size()-1).charAt(0))==sourceX+1
    			&&Input.changeInputToCoordinate(board.getMovedList().get(board.getMovedList().size()-1).charAt(3))==sourceX+1&&board.getFigure(sourceX+1, sourceY)!=null
    					&&board.getFigure(sourceX+1, sourceY).getName().equals("P")) {
			
			//set the position of white pawn to null
			if(targetX-sourceX==1&&targetY-sourceY==1) {
				board.setFigureNull(sourceX+1, sourceY);
			}
			
			
			this.potentialMove[sourceX+1][sourceY+1]=true;
			
		
			
			}
		
	}
	
	/**
	 * get method of UniCode
	 * @return the UniCode of figures
	 */
	public String getUnicode(){
		return "";
	}
	


	
	
	


}
