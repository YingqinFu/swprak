package chess.model;
	/**
	 * the bishop class
	 * @author Yingqin Fu, Steffen Häusler, Christoph Cohrs-Thiede from Gruppe 20
	 *
	 */
public class Bishop extends Figure{

	/**
	 * Constructor. Calls super constructor.
	 * @param x x-coodi
	 * @param y y-coodi
	 * @param color the color of figures
	 * @param potentialMove the potential move of figures
	 */
	public Bishop(int x, int y, Color color, boolean[][] potentialMove) {
		super(x,y,color, potentialMove);
		this.points = 3;
	}
	
	/**
	 * get-method of board name
	 * if color is "w", board name is in capital letter
	 * if color is "b", board name is in lower case letter
	 * @return boardVisual of the bishop as a String
	 */
	@Override
	public String getName() {
		return this.getColor() == Color.white ? "B" : "b";
		
			
	}
	
	

	/**
	 * get-methode for the Unicode used in the 2D-Gui
	 * @return the unicode symbol of the Figure, depending on its color.
	 */
	@Override
	public String getUnicode() {
		return this.getColor() == Color.white ? "♗" : "♝";
	}
	/**
	 * @param x axis position, y axis position
	 * The bishop has no restrictions in distance for each move, but is limited to diagonal movement.
	 * Bishops, like all other pieces except the knight, cannot jump over other pieces. 
	 */
	@Override
	public void setPotentialMove(int x,int y,ChessBoard board) {
		for (int j = 0; j < 8; j++){
			for (int k = 0; k < 8; k++){
				this.potentialMove[j][k] = false;
			}
		}
		// top left
		setTopLeft(x,y,board);
		
		//down right
		setDownRight(x,y,board);
		
		
		//down left
		setDownLeft(x,y,board);
		
		
		//top right
		setTopRight(x,y,board);
		
		
		
		
	}
	/**
	 * set down left
	 * @param x x-axis
	 * @param y y-axis
	 * @param board the current board
	 */
	public void setDownLeft(int x,int y,ChessBoard board) {
		for (int  i= 1; x-i>=0  && y+i < 8; i++){
			if(board.getFigure(x-i, y+i)==null) {
			this.potentialMove[x-i][y+i] = true;
			}
			else {
				if(board.getFigure(x-i, y+i).getColor()!=board.getFigure(x, y).getColor()) {
					this.potentialMove[x-i][y+i]=true;
					break;
				}
				else{
					break;
				}
			}
		}
		
	}
	/**
	 * set down right
	 * @param x x-axis
	 * @param y y-axis
	 * @param board the current board
	 */
	public void setDownRight(int x,int y,ChessBoard board) {
		for (int  i= 1; i+x < 8  && i+y < 8; i++){
			if(board.getFigure(x+i, y+i)==null) {
			this.potentialMove[x+i][y+i] =  true;
			}
			else {
				if(board.getFigure(x+i, y+i).getColor()!=board.getFigure(x, y).getColor()) {
					this.potentialMove[x+i][y+i]=true;
					break;
				}
				else{
					break;
				}
			}
		}
		
	}
	/**
	 * set top right
	 * @param x x-axis
	 * @param y y-axis
	 * @param board the current board
	 */
	public void setTopRight(int x,int y,ChessBoard board) {
		for (int  i= 1; i+x < 8  && y-i>=0; i++){
			if(board.getFigure(x+i, y-i)==null) {
			this.potentialMove[x+i][y-i] =  true;
			}
			else {
				if(board.getFigure(x+i, y-i).getColor()!=board.getFigure(x, y).getColor()) {
					this.potentialMove[x+i][y-i]=true;
					break;
				}
				else{
					break;
				}
			}
		}
		
	}
	/**
	 * set top left
	 * @param x x-axis
	 * @param y y-axis
	 * @param board the current board
	 */
	public void setTopLeft(int x,int y,ChessBoard board) {
		for (int i=1; x-i >= 0 && y-i>=0; i++){
			if(board.getFigure(x-i, y-i)==null) {
			this.potentialMove[x-i][y-i] =  true;
			}
			
			else {
				if(board.getFigure(x-i, y-i).getColor()!=board.getFigure(x, y).getColor()) {
					this.potentialMove[x-i][y-i]=true;
					break;
				}
				else{
					break;
				}
			}
		}
		
	}
	
}
