package chess.model;

import chess.controller.Input;

/**
     * the king class
     * @author Yingqin Fu, Steffen Häusler, Christoph Cohrs-Thiede from Gruppe 20
	
     */
public class King extends Figure{
	/**
	 * Constructor. Calls super constructor.
	 * @param x x-coodi
	 * @param y y-coodi
	 * @param color the color of figures
	 * @param potentialMove the potential move of figures
	 */
	public King(int x,int y,Color color,boolean[][] potentialMove) {
		super(x,y,color,potentialMove);
		this.points = 1;
	}
	
	/**
	 * get-method of board name
	 * if color is "w",  board name is in capital letter
	 * if color is "b",  board name is in lower case letter
	 * @return boardVisual of the king as a String
	 */
	@Override
	public String getName() {
		return this.getColor() == Color.white ? "K" : "k";
	}
	
	
	
	/**
	 * get-methode for the Unicode used in the 2D-Gui
	 * @return the unicode symbol of the Figure, depending on its color.
	 */
	@Override
	public String getUnicode() {
		
		return this.getColor() == Color.white ? "♔" : "♚";
		
	}
	/**
	 * @param x axis position, y axis position
	 * A king can move one square in any direction (horizontally, vertically, or diagonally), 
	 * unless the square is already occupied by a friendly piece, or the move would place the king in check. 
	 */
	@Override
	public void setPotentialMove(int x,int y,ChessBoard board) {
		for (int j = 0; j < 8; j++) {
			for (int k = 0; k < 8; k++) {
				this.potentialMove[j][k] = false;
			}
		}
		
		//set the null position as true
		setPosSpe(x,y,board);
	
	}
	

	/**
	 * set the null position as true
	 * @param x x-axis
	 * @param y y-axis
	 * @param board the current board
	 */
	public void setPosSpe(int x,int y,ChessBoard board) {
		
		int[] xarr = {x + 1, x + 1, x + 1, x, x, x - 1, x - 1, x - 1};
		int[] yarr = {y + 1, y, y - 1, y + 1, y - 1, y + 1, y, y - 1};
		for(int i = 0; i < 8; i++){
			
			boolean basic =xarr[i]>=0 && xarr[i]<8 && yarr[i]>=0 && yarr[i]<8;
			
			if(basic&&(board.getFigure(xarr[i], yarr[i])==null
					||board.getFigure(xarr[i], yarr[i])!=null&&board.getFigure(xarr[i], yarr[i]).getColor()!=
					 board.getFigure(x, y).getColor())
) {
				
				super.potentialMove[xarr[i]][yarr[i]] = true;
		
			
		   }

        }
	}
	
	
	/**
	 * the castling move of white
	 * @param input the input of users
	 * @param board the current board
	 */
	@Override
	public void whiteCastling(Input input,ChessBoard board){
		//short castling 
		whiteShortCastling(input,board);
		//long castling
		whiteLongCastling(input,board);
	
	}
	
	
	/**
	 * the castling move of white
	 * @param input  the input of users
	 * @param board the current board
	 */
	@Override
	public void blackCastling(Input input,ChessBoard board){
		//short castling 
		blackShortCastling(input,board);
		//long castling
		blackLongCastling(input,board);
	
	}
	
	/**
	 * the short castling move of white
	 * @param input  the input of users
	 * @param board the current board
	 */
	public void whiteShortCastling(Input input,ChessBoard board) {
		
		if(input.getInput().equals("e1-g1")&&this.getName().equals("K")&&
		   board.getFigure(6, 7)==null&&board.getFigure(5, 7)==null
		   &&!new King(4,7,Color.white, potentialMove).hasMoved(board)
		   &&!new Rook(7,7,Color.white, potentialMove).hasMoved(board)) {
					
					board.setFigure(5, 7, new Rook(5,7,Color.white, potentialMove));
					this.potentialMove[6][7]=true;
					board.setFigureNull(7, 7);
					
					
				}
		
		
	}
	
	/**
	 * the long castling move of white
	 * @param input  the input of users
	 * @param board the current board
	 */
	public void whiteLongCastling(Input input,ChessBoard board) {
		
		if(input.getInput().equals("e1-c1")&&this.getName().equals("K")
		   &&board.getFigure(1, 7)==null&&board.getFigure(2, 7)==null&&board.getFigure(3, 7)==null
	       &&!new King(4,7,Color.white, potentialMove).hasMoved(board)
	       &&!new Rook(0,7,Color.white, potentialMove).hasMoved(board)) {
					
					board.setFigure(3, 7, new Rook(3,7,Color.white, potentialMove));
					this.potentialMove[2][7]=true;
					board.setFigureNull(0, 7);
				}
		
	}
	
	/**
	 * the short castling move of black
	 * @param input  the input of users
	 * @param board the current board
	 */
	public void blackShortCastling(Input input,ChessBoard board) {
		
		if(input.getInput().equals("e8-g8")&&this.getName().equals("k")
			&&board.getFigure(5, 0)==null&&board.getFigure(6, 0)==null
			&&!new King(4,0,Color.black, potentialMove).hasMoved(board)
			&&!new Rook(7,0,Color.black, potentialMove).hasMoved(board)) {
		
		board.setFigure(5, 0, new Rook(5,0,Color.black, potentialMove));
		this.potentialMove[6][0]=true;
		board.setFigureNull(7, 0);
		
	   }
		
		
	}
	
	/**
	 * the long castling move of black
	 * @param input  the input of users
	 * @param board the current board
	 */
	public void blackLongCastling(Input input,ChessBoard board) {
		
		if(input.getInput().equals("e8-c8")&&this.getName().equals("k")
				&&board.getFigure(1, 0)==null&&board.getFigure(2, 0)==null&&board.getFigure(3, 0)==null
				&&!new King(4,0,Color.black, potentialMove).hasMoved(board)
				&&!new Rook(0,0,Color.black, potentialMove).hasMoved(board)) {
			
			board.setFigure(3, 0, new Rook(3,0,Color.black, potentialMove));
			this.potentialMove[2][0]=true;
			board.setFigureNull(0, 0);
			
		}
		
		
	}

	
	
	
	
	
	
	
	

}
