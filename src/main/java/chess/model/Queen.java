package chess.model;
/**
 * THe queen class
 * @author Yingqin Fu, Steffen Häusler, Christoph Cohrs-Thiede from Gruppe 20
 *
 */
public class Queen extends Figure{

	
/**
 * Constructor. Calls super constructor.
 * @param x x-coodi
 * @param y y-coodi
 * @param color the color of figures
 * @param potentialMove the potential move of figures
 */ 
	public Queen(int x,int y,Color color,boolean[][] potentialMove) {
		super(x,y,color,potentialMove);
		this.points = 9;
	}
	
	/**
	 * get-method of board name
	 * if color is "w", name is in capital letter
	 * if color is "b", name is in lower case letter
	 * @return boardVisual of the queen as a String
	 */
	@Override
	public String getName() {
		
		return this.getColor() == Color.white ? "Q" : "q";
			
	}
	
	
	/**
	 * get-methode for the Unicode used in the 2D-Gui
	 * @return the unicode symbol of the Figure, depending on its color.
	 */
	@Override
	public String getUnicode() {
		return this.getColor() == Color.white ? "♕" : "♛";

	}
	
	/**
	 * @param x axis position, y axis position
	 * set-method of the potential move of Queen(with the right Rules)
	 * The queen can be moved any number of unoccupied squares in a straight line vertically, 
	 * horizontally, or diagonally, thus combining the moves of the rook and bishop.
	 */
	@Override
	public void setPotentialMove(int x,int y,ChessBoard board){
		for (int j = 0; j < 8; j++){
			for (int k = 0; k < 8; k++){
				this.potentialMove[j][k] = false;
			}
		
		}
		
		Boolean[][] bMoves = new Boolean[8][8];
		Boolean[][] rMoves = new Boolean[8][8];
		Rook r = new Rook(x,y,getColor(),potentialMove);
		Bishop b = new Bishop(x,y,getColor(),potentialMove);
		
		//get the potential move of bishop
		b.setPotentialMove(x, y,board);	
		for (int j = 0; j < 8; j++){
			for (int k = 0; k < 8; k++){
				bMoves[j][k] = b.getPotentialMove()[j][k];
			}
		}
		//get the potential move of rook
		r.setPotentialMove(x, y,board);
		for (int j = 0; j < 8; j++){
			for (int k = 0; k < 8; k++){
				rMoves[j][k] = r.getPotentialMove()[j][k];
			}
		}
		//combining the moves of the rook and bishop
		for (int j = 0; j < 8; j++){
			for (int k = 0; k < 8; k++){
				this.potentialMove[j][k] = rMoves[j][k] || bMoves[j][k];
			}
		}


		
	}
}
