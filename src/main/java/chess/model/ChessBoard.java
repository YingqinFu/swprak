
package chess.model;

import java.util.ArrayList;
import java.util.List;

/**
	 * the class,to definite the details of the chess board
	 * @author Yingqin Fu, Steffen Häusler, Christoph Cohrs-Thiede from Gruppe 20
	 *
	 */
public class ChessBoard {
	
	/**
	 * String for the print of board
	 */
	private String field;
	/**
	 * Array for all positions as Figures objects
	 */
	private String[][] name = new String[8][8];
	/**
	 * Array for all board symbols as String
	 */
	private Figure[][] chess =new Figure[8][8];
	
	/**
	 * ArrayList for all the move from users
	 */
	private List<String> movedList = new ArrayList<String>();
	/**
	 * ArrayList for all the beaten figures
	 */
	private List<String> beatenList = new ArrayList<String>();
	
	/**
	 * integer that stores the first selected field by the player
	 */
	private String selectedField = "";
	/**
	 * boolean for identifying the launch mode
	 */
	private boolean guiMode = false;
	/**
	 * boolean for identifying the gamemode
	 */
	private boolean pvp = false;
	/**
	 * String for the print of board
	 */
	private boolean turnField = false;
	/**
	 * boolean to toggle the Button showMoves
	 */

	private boolean showMoves = false;
	/**
	 * boolean to toggle the Button showInCheck
	 */

	private boolean showInCheck = false;
	/**
	 * boolean to toggle the Button reselect
	 */

	private boolean reselect = false;
	/**
	 * boolean to save if the last move made was valid
	 */

	private boolean lastMoveValid = false;

	/**
	 * boolean used to finish the Game by checkmate
	 */
	private boolean Checkmate = false;

	/**
	 * boolean used to finish the Game by draw
	 */
	private boolean draw = false;

	/**
	 * initialize the chess board
	 * @param figures the 2d-array for figures
	 */
	public ChessBoard(Figure[][] figures){
		Figure [][] copy=new Figure[8][8];
		copyFigure(figures,copy);
		
		this.chess = copy;
	}
	/**
	 * constructor
	 */
	public ChessBoard(){

	}
	/**
	 * to store the array
	 * @param figure the 2d-array
	 * @param copy the 2d-array to be stored
	 */
	public void copyFigure(Figure[][] figure,Figure[][] copy) {
		
		for (int j = 0; j < 8; j++){
			for (int k = 0; k < 8; k++){
				copy[j][k]=figure[j][k];
			}
		}
		
	}
	/**
	 * initialize the whole Chess board
	 */
	public void initializeBoard() {
		
		// gets boardVisual of figures if existing
		for(int x =0; x<8;x++) {
			for(int y =0; y<8;y++) {
				
				if(chess[x][y] == null) {
					name[x][y] = " ";
				}
			}
			
		} 
		this.field = "8"+" "+  name[0][0] +" "+  name[1][0] +" "+  name[2][0] +" "+  name[3][0] +" "+ name[4][0] +" "+  name[5][0] +" "+  name[6][0] +" "+  name[7][0] + "\n"
				  + "7"+" "+  name[0][1] +" "+  name[1][1] +" "+  name[2][1] +" "+  name[3][1] +" "+ name[4][1] +" "+  name[5][1] +" "+  name[6][1] +" "+  name[7][1] +"\n"
				  + "6"+" "+  name[0][2] +" "+  name[1][2] +" "+  name[2][2] +" "+  name[3][2] +" "+ name[4][2] +" "+  name[5][2] +" "+  name[6][2] +" "+  name[7][2]+"\n"
				  + "5"+" "+  name[0][3] +" "+  name[1][3] +" "+  name[2][3] +" "+  name[3][3] +" "+ name[4][3] +" "+  name[5][3] +" "+  name[6][3] +" "+  name[7][3] +"\n"
				  + "4"+" "+  name[0][4] +" "+  name[1][4] +" "+  name[2][4] +" "+  name[3][4] +" "+ name[4][4] +" "+  name[5][4] +" "+  name[6][4] +" "+  name[7][4] +"\n"
				  + "3"+" "+  name[0][5] +" "+  name[1][5] +" "+  name[2][5] +" "+  name[3][5] +" "+ name[4][5] +" "+  name[5][5] +" "+  name[6][5] +" "+  name[7][5] +"\n"
				  + "2"+" "+  name[0][6] +" "+  name[1][6] +" "+  name[2][6] +" "+  name[3][6] +" "+ name[4][6] +" "+  name[5][6] +" "+  name[6][6] +" "+  name[7][6] +"\n"
				  + "1"+" "+  name[0][7] +" "+  name[1][7] +" "+  name[2][7] +" "+  name[3][7] +" "+ name[4][7] +" "+  name[5][7] +" "+  name[6][7] +" "+  name[7][7] +"\n"
				  + " "+" a"+" b"+" c"+ " d"+" e"+" f"+" g"+" h";
	}
	
	
	
	/**
	 * get-method of the field
	 * @return field the field of board
	 */
	public String getField(){
		return this.field;
	}
   
   
	/**
	 * set-method of the figures
	 * @param x x-coodi
	 * @param y y-coodi
	 * @param figure the figures
	 */
	public void setFigure(int x, int y, Figure figure) {
		
		chess[x][y]=figure;
		
		this.name[x][y] = figure.getName();
		
		
	}
	
	
	/**
	 * get-method of the figures
	 * @param x x-axis
	 * @param y y-axis
	 * @return chess the 2d-Array for figures
	 */
	public Figure getFigure(int x,int y){
		return this.chess[x][y];
		
		
   }

	
	/**
	 * set the figures to null
	 * @param x x-axis
	 * @param y y-axis
	 */
	public void setFigureNull(int x, int y) {

		chess[x][y]=null;
		
	}
	
	
	/**
	 * get-method of move list
	 * @return movedList the movedList
	 */
	public List<String> getMovedList(){
		return this.movedList;
	}
	/**
	 * add the move to move list
	 * @param move the move of users
	 */
	public void setMovedList(String move) {
		this.movedList.add(move);
	}
	
	/**
	 * get-method of beat list
	 * @return beatenList the beaten list
	 */
	public List<String> getBeatenList(){
		return this.beatenList;
	}
	
	
	
	/**
	 * getter for isGuiMode
	 * @return guiMode guiMode or not
	 */
	public boolean isGuiMode() {
		return guiMode;
	}

	/**
	 * setter for gui mode
	 * @param guiMode the guiMode
	 */
	public void setGuiMode(boolean guiMode) {
		this.guiMode = guiMode;
	}

	/**
	 * getter for pvp mode
	 * @return pvp pvp mode
	 */
	public boolean isPvp() {
		return pvp;
	}

	/**
	 * setter for pvp mode
	 * @param pvp pvp mode
	 */
	public void setPvp(boolean pvp) {
		this.pvp = pvp;
	}
	/**
	 * get-method of the selected Field
	 * @return selectedField field selected or not
	 */

	public String getSelectedField() {
		return selectedField;
	}
	/**
	 * set-method of selected Field
	 * @param selectedField field selected or not
	 */

	public void setSelectedField(String selectedField) {
		this.selectedField = selectedField;
	}
	/**
	 *  convert the ChessFigure
	 * @param fig The ChessFigure that need to be converted, the Name can be found in Figure.getName()
	 * @return The Figures corresponding UFT-8 Symbol
	 */

	public String toUTF8(String fig){
		String cslFig = "pPrRnNbBqQkK";
		String uft8Fig = "♟♙♜♖♞♘♝♗♛♕♚♔";
		String output = "";
		for (int i = 0; i<12; i++){
			if (cslFig.charAt(i) == fig.charAt(0))
				output = String.valueOf(uft8Fig.charAt(i));
		}
		return output;

	}
	/**
	 * check if turn field or not
	 * @return turnField make field turn
	 */

	public boolean isTurnField() {
		return turnField;
	}
	/**
	 * set method of turn field
	 * @param turnField make field turn
	 */

	public void setTurnField(boolean turnField) {
		this.turnField = turnField;
	}
	/**
	 * get method of showMoves
	 * @return showMoves show all the moves
	 */

	public boolean isShowMoves() {
		return showMoves;
	}
	/**
	 * set method of showMoves
	 * @param showMoves show all the moves
	 */

	public void setShowMoves(boolean showMoves) {
		this.showMoves = showMoves;
	}
	/**
	 * get method of ShowInChess
	 * @return showInCheck show a king in check or not
	 */

	public boolean isShowInCheck() {
		return showInCheck;
	}



	/**
	 * set method of ShowInChess
	 * @param showInCheck  show a king in check or not
	 */

	public void setShowInCheck(boolean showInCheck) {
		this.showInCheck = showInCheck;
	}

	/**
	 * get method of reselect
	 * @return reselect the reselect of users
	 */

	public boolean isReselect() {
		return reselect;
	}
	/**
	 * set method of reselect
	 * @param reselect  the reselect of users
	 */

	public void setReselect(boolean reselect) {
		this.reselect = reselect;
	}
	/**
	 * get method of lastMoveValid
	 * @return lastMoveValid the last move valid or not
	 */

	public boolean isLastMoveValid() {
		return lastMoveValid;
	}
	/**
	 * set method of lastMoveValid
	 * @param lastMoveValid the last move valid or not
	 */

	public void setLastMoveValid(boolean lastMoveValid) {
		this.lastMoveValid = lastMoveValid;
	}
	
	/**
	 * getter of Checkmate
	 * @return true if checkmate
	 */
	public boolean isCheckmate() {
		return Checkmate;
	}

	/**
	 * setter for checkmate
	 * @param checkmate set to true if checkmate is detected
	 */
	public void setCheckmate(boolean checkmate) {
		Checkmate = checkmate;
	}

	/**
	 * getter of Draw
	 * @return true if draw detected
	 */
	public boolean isDraw() {
		return draw;
	}

	/**
	 * setter of draw
	 * @param draw set to true if draw detected
	 */
	public void setDraw(boolean draw) {
		this.draw = draw;
	}


	
	
}
