package chess.model;
	/**
	 * the rook class
	 * @author Yingqin Fu, Steffen Häusler, Christoph Cohrs-Thiede from Gruppe 20
	 *
	 */
public class Rook extends Figure{
	/**
	 * Constructor. Calls super constructor.
	 * @param x x-coodi
	 * @param y y-coodi
	 * @param color the color of users
	 * @param potentialMove the potential move of figures
	 */
	public Rook(int x,int y,Color color,boolean[][] potentialMove) {
		super(x,y,color,potentialMove);
		this.points = 5;
	}
	
	
	/**
	 * get-method of board name
	 * if color is "w", board name is in capital letter
	 * if color is "b", board name is in lower case letter
	 * @return boardVisual of the rook as a String
	 */
	@Override
	public String getName() {
	
		return this.getColor() == Color.white ? "R" : "r";
			
	}
	

	/**
	 * get-methode for the Unicode used in the 2D-Gui
	 * @return the unicode symbol of the Figure, depending on its color.
	 */
	@Override
	public String getUnicode() {
		
		return this.getColor() == Color.white ? "♖" : "♜";

	}

	/**
	 * @param x axis position, y axis position
	 * The rook moves horizontally or vertically. 
	 * The rook cannot jump over pieces.
	 */
	@Override
	public void setPotentialMove(int x,int y,ChessBoard board){
		for (int j = 0; j < 8; j++){
			for (int k = 0; k < 8; k++){
				this.potentialMove[j][k] = false;
			}
		}
		//left horizontally
		setLeftHorizontally(x,y,board);
		
		//right horizontally
		setRightHorizontally(x,y,board);
		
		
		//top vertically
		setTopVertically(x,y,board);
		
		
		//down vertically
		setDownVertically(x, y, board);
	}
	
	/**
	 * set Left horizontally
	 * @param x x-axis
	 * @param y y-axis
	 * @param board the current board
	 */
	public void setLeftHorizontally(int x, int y, ChessBoard board) {
		for(int i = x-1; i>=0 ; i--){
			if(board.getFigure(i, y)==null) {
			
				super.potentialMove[i][y] = true;
			}
			else {
				//A rook can beat the first enemy figure he meet
				if(board.getFigure(i, y).getColor()!=board.getFigure(x, y).getColor()) {
					super.potentialMove[i][y]=true;
					break;
				}
				else{
					break;
				}
			}
	
		}
		
	}
	
	
	
	/**
	 * set right horizontally
	 * @param x x-axis
	 * @param y y-axis
	 * @param board the current board
	 */
	public void setRightHorizontally(int x, int y, ChessBoard board) {
		for(int i = x+1; i < 8 ; i++){
			if(board.getFigure(i, y)==null) {
			super.potentialMove[i][y] = true;
			
			}
			else {
				if(board.getFigure(i, y).getColor()!=board.getFigure(x, y).getColor()) {
					this.potentialMove[i][y]=true;
					
					break;
				}
				else{
					break;
				}
			}
		}
		
	}
	
	
	
	/**
	 * set top horizontally
	 * @param x x-axis
	 * @param y y-axis
	 * @param board the current board
	 */
	public void setTopVertically(int x, int y, ChessBoard board) {
		for(int i = y-1; i >=0; i--){
			if(board.getFigure(x, i)==null) {
			this.potentialMove[x][i] = true;
			}
			else {
				if(board.getFigure(x, i).getColor()!=board.getFigure(x, y).getColor()) {
					this.potentialMove[x][i]=true;
					break;
				}
				else{
					break;
				}
			}
		}
		
		
	}
	
	
	
	/**
	 * set down Vertically
	 * @param x x-axis
	 * @param y y-axis
	 * @param board the current board
	 */
	public void setDownVertically(int x, int y, ChessBoard board) {
		for(int i = y+1; i < 8 ; i++){
			if(board.getFigure(x, i)==null) {
			this.potentialMove[x][i] = true;
			}
			else {
				if(board.getFigure(x, i).getColor()!=board.getFigure(x, y).getColor()) {
					this.potentialMove[x][i]=true;
					break;
				}
				else{
					break;
				}
			}
		}
		
		
	}
	


}
